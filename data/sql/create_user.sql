CREATE USER 'plantilla-buap'@'localhost' IDENTIFIED BY 'LA5IBIvE';
GRANT USAGE ON *.* TO 'plantilla-buap'@'localhost';
GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `dev-drupal`.* TO 'plantilla-buap'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
