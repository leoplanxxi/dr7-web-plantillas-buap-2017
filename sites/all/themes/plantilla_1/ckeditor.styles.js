/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
if(typeof(CKEDITOR) !== 'undefined') {
    CKEDITOR.addStylesSet( 'drupal',
    [
            /* Block Styles */

            // These styles are already available in the 'Format' drop-down list, so they are
            // not needed here by default. You may enable them to avoid placing the
            // 'Format' drop-down list in the toolbar, maintaining the same features.
            /*
            { name : 'Paragraph'		, element : 'p' },
            { name : 'Heading 1'		, element : 'h1' },
            { name : 'Heading 2'		, element : 'h2' },
            { name : 'Heading 3'		, element : 'h3' },
            { name : 'Heading 4'		, element : 'h4' },
            { name : 'Heading 5'		, element : 'h5' },
            { name : 'Heading 6'		, element : 'h6' },
            { name : 'Preformatted Text', element : 'pre' },
            { name : 'Address'			, element : 'address' },
            */

            /*Títulos Azul fuerte*/
            { name: 'Título Negrita Azul Fuerte H1', 					  element: 'h1', styles: { 'font-size': '22px',	'color': '#004C6C', 'font-weight': '600' } },
          	{ name: 'Título Azul Fuerte H1', 										element: 'h1', styles: { 'font-size': '22px',	'color': '#004C6C', 'font-weight': '400' } },
          	{ name: 'Título Negrita Azul Fuerte H2', 						element: 'h2', styles: { 'font-size': '20px',	'color': '#004C6C', 'font-weight': '600' } },
            { name: 'Título Azul Fuerte H2', 										element: 'h2', styles: { 'font-size': '20px',	'color': '#004C6C', 'font-weight': '400' } },
            { name: 'Subtítulo Negrita Azul Fuerte H3', 				element: 'h3', styles: { 'font-size': '18px',	'color': '#004C6C', 'font-weight': '600' } },
          	{ name: 'Subtítulo Azul Fuerte H3', 								element: 'h3', styles: { 'font-size': '18px',	'color': '#004C6C', 'font-weight': '400' } },
          	{ name: 'Subtítulo Light Azul Fuerte H3', 					element: 'h3', styles: { 'font-size': '18px',	'color': '#004C6C', 'font-weight': '200' } },
          	{ name: 'Subtítulo Light Cursiva Azul Fuerte H3',		element: 'h3', styles: { 'font-size': '18px',	'color': '#004C6C', 'font-weight': '200', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Cursiva Negrita Azul Fuerte H3',	element: 'h3', styles: { 'font-size': '18px',	'color': '#004C6C', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Cursiva Azul Fuerte H3',					element: 'h3', styles: { 'font-size': '18px',	'color': '#004C6C', 'font-weight': '400', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Negrita Azul Fuerte H4',					element: 'h4', styles: { 'font-size': '16px',	'color': '#004C6C', 'font-weight': '600' } },
          	{ name: 'Subtítulo Azul Fuerte H4',									element: 'h4', styles: { 'font-size': '16px',	'color': '#004C6C', 'font-weight': '400' } },
          	{ name: 'Subtítulo Cursiva Negrita Azul Fuerte H4',	element: 'h4', styles: { 'font-size': '16px',	'color': '#004C6C', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Cursiva Azul Fuerte H4',					element: 'h4', styles: { 'font-size': '16px',	'color': '#004C6C', 'font-weight': '400', 'font-style': 'italic' } },
          	{ name: 'Texto Negrita Azul Fuerte H5',							element: 'h5', styles: { 'font-size': '14px',	'color': '#004C6C', 'font-weight': '600' } },
          	{ name: 'Texto Azul Fuerte H5',											element: 'h5', styles: { 'font-size': '14px',	'color': '#004C6C', 'font-weight': '400' } },
          	{ name: 'Texto Cursiva Negrita Azul Fuerte H5',			element: 'h5', styles: { 'font-size': '14px',	'color': '#004C6C', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Texto Cursiva Azul Fuerte H5',							element: 'h5', styles: { 'font-size': '14px',	'color': '#004C6C', 'font-weight': '400', 'font-style': 'italic' } },
          	{ name: 'Texto Bold Azul Fuerte H5',								element: 'h5', styles: { 'font-size': '14px',	'color': '#004C6C', 'font-weight': '700' } },
          	{ name: 'Texto Bold Cursiva Azul Fuerte H5',				element: 'h5', styles: { 'font-size': '14px',	'color': '#004C6C', 'font-weight': '700', 'font-style': 'italic' } },
          	{ name: 'Nota Negrita Azul Fuerte H6',							element: 'h6', styles: { 'font-size': '12px',	'color': '#004C6C', 'font-weight': '600' } },
          	{ name: 'Nota Azul Fuerte H6',											element: 'h6', styles: { 'font-size': '12px',	'color': '#004C6C', 'font-weight': '400' } },
          	{ name: 'Nota Cursiva Negrita Azul Fuerte H6',			element: 'h6', styles: { 'font-size': '12px',	'color': '#004C6C', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Nota Cursiva Azul Fuerte H6',							element: 'h6', styles: { 'font-size': '12px',	'color': '#004C6C', 'font-weight': '400', 'font-style': 'italic' } },

          	/* Títulos Azul Claro */
          	{ name: 'Título Negrita Azul Claro H1', 						element: 'h1', styles: { 'font-size': '22px',	'color': '#00B3E3', 'font-weight': '600' } },
          	{ name: 'Título Azul Claro H1', 										element: 'h1', styles: { 'font-size': '22px',	'color': '#00B3E3', 'font-weight': '400' } },
          	{ name: 'Título Negrita Azul Claro H2', 						element: 'h2', styles: { 'font-size': '20px',	'color': '#00B3E3', 'font-weight': '600' } },
            { name: 'Título Azul Claro H2', 										element: 'h2', styles: { 'font-size': '20px',	'color': '#00B3E3', 'font-weight': '400' } },
            { name: 'Subtítulo Negrita Azul Claro H3', 					element: 'h3', styles: { 'font-size': '18px',	'color': '#00B3E3', 'font-weight': '600' } },
          	{ name: 'Subtítulo Azul Claro H3', 									element: 'h3', styles: { 'font-size': '18px',	'color': '#00B3E3', 'font-weight': '400' } },
          	{ name: 'Subtítulo Light Azul Claro H3', 						element: 'h3', styles: { 'font-size': '18px',	'color': '#00B3E3', 'font-weight': '200' } },
          	{ name: 'Subtítulo Light Cursiva Azul Claro H3',		element: 'h3', styles: { 'font-size': '18px',	'color': '#00B3E3', 'font-weight': '200', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Cursiva Negrita Azul Claro H3',	element: 'h3', styles: { 'font-size': '18px',	'color': '#00B3E3', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Cursiva Azul Claro H3',					element: 'h3', styles: { 'font-size': '18px',	'color': '#00B3E3', 'font-weight': '400', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Negrita Azul Claro H4',					element: 'h4', styles: { 'font-size': '16px',	'color': '#00B3E3', 'font-weight': '600' } },
          	{ name: 'Subtítulo Azul Claro H4',									element: 'h4', styles: { 'font-size': '16px',	'color': '#00B3E3', 'font-weight': '400' } },
          	{ name: 'Subtítulo Cursiva Negrita Azul Claro H4',	element: 'h4', styles: { 'font-size': '16px',	'color': '#00B3E3', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Subtítulo Cursiva Azul Claro H4',					element: 'h4', styles: { 'font-size': '16px',	'color': '#00B3E3', 'font-weight': '400', 'font-style': 'italic' } },
          	{ name: 'Texto Negrita Azul Claro H5',							element: 'h5', styles: { 'font-size': '14px',	'color': '#00B3E3', 'font-weight': '600' } },
          	{ name: 'Texto Azul Claro H5',											element: 'h5', styles: { 'font-size': '14px',	'color': '#00B3E3', 'font-weight': '400' } },
          	{ name: 'Texto Cursiva Negrita Azul Claro H5',			element: 'h5', styles: { 'font-size': '14px',	'color': '#00B3E3', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Texto Cursiva Azul Claro H5',							element: 'h5', styles: { 'font-size': '14px',	'color': '#00B3E3', 'font-weight': '400', 'font-style': 'italic' } },
          	{ name: 'Texto Bold Azul Claro H5',									element: 'h5', styles: { 'font-size': '14px',	'color': '#00B3E3', 'font-weight': '700' } },
          	{ name: 'Texto Bold Cursiva Azul Claro H5',					element: 'h5', styles: { 'font-size': '14px',	'color': '#00B3E3', 'font-weight': '700', 'font-style': 'italic' } },
          	{ name: 'Nota Negrita Azul Claro H6',								element: 'h6', styles: { 'font-size': '12px',	'color': '#00B3E3', 'font-weight': '600' } },
          	{ name: 'Nota Azul Claro H6',												element: 'h6', styles: { 'font-size': '12px',	'color': '#00B3E3', 'font-weight': '400' } },
          	{ name: 'Nota Cursiva Negrita Azul Claro H6',				element: 'h6', styles: { 'font-size': '12px',	'color': '#00B3E3', 'font-weight': '600', 'font-style': 'italic' } },
          	{ name: 'Nota Cursiva Azul Claro H6',								element: 'h6', styles: { 'font-size': '12px',	'color': '#00B3E3', 'font-weight': '400', 'font-style': 'italic' } },

            /* Títulos gris */
            { name: 'Texto Negrita Gris H5',                    element: 'h5', styles: { 'font-size': '14px', 'color': '#5a5959', 'font-weight': '600' } },
            { name: 'Texto Gris H5',                            element: 'h5', styles: { 'font-size': '14px', 'color': '#5a5959', 'font-weight': '400' } },
            { name: 'Texto Cursiva Negrita Gris H5',            element: 'h5', styles: { 'font-size': '14px', 'color': '#5a5959', 'font-weight': '600', 'font-style': 'italic' } },
            { name: 'Texto Cursiva Gris H5',                    element: 'h5', styles: { 'font-size': '14px', 'color': '#5a5959', 'font-weight': '400', 'font-style': 'italic' } },
            { name: 'Texto Bold Gris H5',                       element: 'h5', styles: { 'font-size': '14px', 'color': '#5a5959', 'font-weight': '700' } },
            { name: 'Texto Bold Cursiva Gris H5',               element: 'h5', styles: { 'font-size': '14px', 'color': '#5a5959', 'font-weight': '700', 'font-style': 'italic' } },
            { name: 'Nota Negrita Gris H6',                     element: 'h6', styles: { 'font-size': '12px', 'color': '#5a5959', 'font-weight': '600' } },
            { name: 'Nota Gris H6',                             element: 'h6', styles: { 'font-size': '12px', 'color': '#5a5959', 'font-weight': '400' } },
            { name: 'Nota Cursiva Negrita Gris H6',             element: 'h6', styles: { 'font-size': '12px', 'color': '#5a5959', 'font-weight': '600', 'font-style': 'italic' } },
            { name: 'Nota Cursiva Gris H6',                     element: 'h6', styles: { 'font-size': '12px', 'color': '#5a5959', 'font-weight': '400', 'font-style': 'italic' } },

            /* Viñetas */
            { name: 'Viñeta 01 Números Decimal',                        element: 'ol', attributes: { 'class' : 'vineta_01_numeros_decilmal' } },
            { name: 'Viñeta 01 Números Decimal Cursiva',                element: 'ol', attributes: { 'class' : 'vineta_01_numeros_decilmal_cursiva' } },
            { name: 'Viñeta 02 Números Cero Más Decimal',               element: 'ol', attributes: { 'class' : 'vineta_02_numeros_zero_mas_decilmal' } },
            { name: 'Viñeta 02 Números Cero Más Decimal Cursiva',       element: 'ol', attributes: { 'class' : 'vineta_02_numeros_zero_mas_decilmal_cursiva' } },
            { name: 'Viñeta 03 Números Romanos Mayúsculas',             element: 'ol', attributes: { 'class' : 'vineta_03_numeros_romanos_mayusculas' } },
            { name: 'Viñeta 03 Números Romanos Mayúsculas Cursiva',     element: 'ol', attributes: { 'class' : 'vineta_03_numeros_romanos_mayusculas_cursiva' } },
            { name: 'Viñeta 04 Números Romanos Minúsculas',             element: 'ol', attributes: { 'class' : 'vineta_04_numeros_romanos_minusculas' } },
            { name: 'Viñeta 04 Números Romanos Minúsculas Cursiva',     element: 'ol', attributes: { 'class' : 'vineta_04_numeros_romanos_minusculas_cursiva' } },
            { name: 'Viñeta 05 Numeración Alfabeto Mayúsculas',         element: 'ol', attributes: { 'class' : 'vineta_05_numeracion_alfabeto_mayusculas' } },
            { name: 'Viñeta 05 Numeración Alfabeto Mayúsculas Cursiva', element: 'ol', attributes: { 'class' : 'vineta_05_numeracion_alfabeto_mayusculas_cursiva' } },
            { name: 'Viñeta 06 Numeración Alfabeto Minúsculas',         element: 'ol', attributes: { 'class' : 'vineta_06_numeracion_alfabeto_minusculas' } },
            { name: 'Viñeta 06 Numeración Alfabeto Minúsculas Cursiva', element: 'ol', attributes: { 'class' : 'vineta_06_numeracion_alfabeto_minusculas_cursiva' } },
            { name: 'Viñeta 07 Cuadrada',                               element: 'ul', attributes: { 'class' : 'vineta_07_cuadrada' } },
            { name: 'Viñeta 07 Cuadrada Cursiva',                       element: 'ul', attributes: { 'class' : 'vineta_07_cuadrada_cursiva' } },

            /* Viñetas imágenes */
            /*{ name: 'Viñeta 08 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_01.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 08 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_01.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
            { name: 'Viñeta 09 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_02.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 09 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_02.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
            { name: 'Viñeta 10 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_03.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 10 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_03.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
            { name: 'Viñeta 11 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_04.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 11 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_04.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
            { name: 'Viñeta 12 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_05.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 12 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_05.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
            { name: 'Viñeta 13 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_06.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 13 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_06.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
            { name: 'Viñeta 14 Imagen 1',                               element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_07.png')', 'margin-left': '5px', 'font-size': '13px' } },
            { name: 'Viñeta 14 Imagen 1 Cursiva',                       element: 'ul', styles: { list-style-image: 'url('ima_venetas/venetas_07.png')', 'margin-left': '5px', 'font-style': 'italic', 'font-size': '13px' } },
*/

            /* Object Styles */

            {
                    name : 'Image on Left',
                    element : 'img',
                    attributes :
                    {
                            'style' : 'padding: 5px; margin-right: 5px',
                            'border' : '2',
                            'align' : 'left'
                    }
            },

            {
                    name : 'Image on Right',
                    element : 'img',
                    attributes :
                    {
                            'style' : 'padding: 5px; margin-left: 5px',
                            'border' : '2',
                            'align' : 'right'
                    }
            }
    ]);
    CKEDITOR.config.stylesSet = 'drupal';
}
