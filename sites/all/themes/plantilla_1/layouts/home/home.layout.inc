name = home
description = Plantilla 1 BUAP - Portada.
preview = preview.png
template = home-layout

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/styles.css

; ========================================
; Scripts
; ========================================
scripts[] = js/behaviors.js

; ========================================
; Regions
; ========================================
regions[menu-buap] = Menu BUAP
regions[menu-secundario] = Menu Secundario
regions[slider-principal] = Slider Principal
; regions[superpanel-ci] = Superpanel CI
; regions[superpanel-cc] = Superpanel CC
; regions[superpanel-cd] = Superpanel CD
regions[lienzo] = Lienzo
regions[noticias-buap] = Noticias BUAP
regions[footer-dependencia] = Footer Dependencia
regions[footer] = Footer