<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['branding']: Items for the branding region.
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see omega_preprocess_page()
 */

function frontal($nidFrontal) {
    require __DIR__ . '/../common/contenido.php';
	require __DIR__ . '/../common/main.php';

    $GLOBALS['i2'] = 0;
    global $base_url;

    drupal_add_css("https://unpkg.com/leaflet@1.3.1/dist/leaflet.css", 'external');
    drupal_add_js('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js', 'external');


    // Variables de configuración
    $mostrar_overlay = config_pages_get('configuraci_n_de_sitio', 'field_mostrar_pesos_en_recursos');
    $c3 = config_pages_get('configuraci_n_de_sitio', 'field_desplegar_3_columnas'); /*1 es que esta seleccionado, 0 o NULL no esta seleccionado */
    $c4 = config_pages_get('configuraci_n_de_sitio', 'field_desplegar_4_columnas');
    $c2 = config_pages_get('configuraci_n_de_sitio', 'field_desplegar_2_columnas');
    $c1 = config_pages_get('configuraci_n_de_sitio', 'field_desplegar_1_columna');
    $noticias = config_pages_get('configuraci_n_de_sitio', 'field_desplegar_panel_noticias_d');
    $superpanel = config_pages_get('configuraci_n_de_sitio', 'field_superpanel_sitio');
	$titulo_documento = config_pages_get('configuraci_n_de_sitio', 'field_nombre_del_sitio');
	$id_analytics = config_pages_get("configuraci_n_de_sitio", "field_id_analytics");
    
    $pesos["c3"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_3_columnas')["value"];
    $pesos["c3v"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_3_cols_var')["value"];
    $pesos["c2"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_2_columnas')["value"];
    $pesos["c2v"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_2_cols_var')["value"];
    $pesos["c4"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_4_columnas')["value"];
    $pesos["c4v"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_4_cols_var')["value"];
    $pesos["c1"] = config_pages_get('configuraci_n_de_sitio', 'field_peso_1_columna')["value"];

    $js_personalizado = config_pages_get('scripts_y_css_personalizados', 'field_javascript');
    $css_personalizado = config_pages_get('scripts_y_css_personalizados', 'field_css');

    // Carga de Recursos
    $array_4col = cuatro_columnas($nidFrontal);
    $array_3col = tres_columnas($nidFrontal);
    $array_2col = dos_columnas($nidFrontal);
    $array_cc = columna_central_superpanel($nidFrontal);
    $array_ci = columna_izquierda_superpanel($nidFrontal);
    $array_cd = columna_derecha_superpanel($nidFrontal);
    $array_1col = una_columna($nidFrontal);
    //$noticias_buap = noticias_buap();

    $array_2colv = dos_columnas_var($nidFrontal);
    $array_3colv = tres_columnas_var($nidFrontal);
    $array_4colv = cuatro_columnas_var($nidFrontal);

    // Ordenar por pesos
    asort($pesos);
    
    echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
    echo '<script>
            document.title = "' . $titulo_documento .'";
            var mapas = [];
        </script>';

    echo "    <!-- Make sure you put this AFTER Leaflet's CSS -->        ";

    echo '<!-- SCRIPTS PERSONALIZADOS -->
    <script>
        ' . $js_personalizado . '
    </script>';

    echo '<!-- ESTILOS PERSONALIZADOS -->

    <style>
        ' . $css_personalizado . '
    </style>';

    echo '<script src="' . $base_url . '/sites/all/libraries/flexslider/jquery.flexslider-min.js"></script>
		<script src="' . $base_url . '/sites/all/libraries/sidr/jquery.sidr.min.js"></script>
		<script async="" src="//www.google-analytics.com/analytics.js"></script>
        <link type="text/css" rel="stylesheet" href="' . $base_url . '/sites/all/libraries/flexslider/flexslider.css" media="all">
        <link type="text/css" rel="stylesheet" href="' . $base_url . '/sites/all/libraries/sidr/stylesheets/jquery.sidr.bare.css" media="all">';

    echo "
    <script>
        jQuery(window).load(function() {
        /*jQuery('.flexslider').flexslider({
            animation: 'slide'
        });*/

        jQuery('#simple-menu').sidr({
            side: 'right'
        });
        });
	</script>";
	
	if ($id_analytics != NULL) {
		echo "<script>
		window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
		ga('create', '" . $id_analytics . "', 'auto');
		ga('send', 'pageview');
		</script>";
	}
    
	echo '<style>';
		if ($noticias != '1') {
			echo ".l-noticias-sitio { display: none; };";
		}
		else {
			echo ".l-noticias-sitio {
				width: 100%;
				overflow: hidden;
				padding-bottom: 25px;
			}";
		}

		if ($mostrar_overlay != '1') {
			echo ".peso { display: none; }";
		}
	echo '</style>';

	echo "<style>";
		echo ".hamburger {
			width: 65px;
			height: 65px;
	
		}
	
		.hamburger a {
			background: url($base_url/sites/all/themes/plantilla_1/images/menu_responsive_blanco.png);
			height: 65px;
			width: 65px;
			background-repeat: no-repeat;
			background-position: center;
		}
		.hamburger a:hover {
			background: url($base_url/sites/all/themes/plantilla_1/images/menu_responsive_azul.png);
		}";
	echo "</style>";
	echo "<div class='container'>";
		echo "<div class='l-page'>";
			echo '<header class="l-header" role="banner">
			<div class="l-branding">
			  <div class="l-menu">
			  <div class="l-region--menu-buap">
			  	<div class="header-logo-buap">
					<a href="http://buap.mx"><img src=" ' . $base_url . '/sites/all/themes/plantilla_1/images/escudo_blanco.png" /></a>
				</div>
		
				<div class="header-redes">
				  <a href="https://www.facebook.com/BUAPoficial/"><img src="' . $base_url . '/sites/all/themes/plantilla_1/images/facebook.png"></a>
				  <a href="https://twitter.com/buapoficial?lang=en"><img src="' . $base_url . '/sites/all/themes/plantilla_1/images/twitter.png"></a>
				  <a href="https://www.instagram.com/buapoficial/"><img src="' . $base_url . '/sites/all/themes/plantilla_1/images/instagram.png"></a>
				  <a href="https://www.youtube.com/user/ibuap"><img src="' . $base_url . '/sites/all/themes/plantilla_1/images/youtube.png"></a>';
					 global $theme;
					 if ($theme != variable_get('admin_theme', 'seven')) {
						$form = drupal_get_form('search_block_form', TRUE);
						   print render($form);
					 } 
		
				echo '</div>
		
				<ul class="menu"><li class="first leaf"><a href="http://www.autoservicios.buap.mx/" target="_blank">Autoservicios</a></li>
				<li class="last leaf"><a href="http://www.correo.buap.mx/" target="_blank">Correo BUAP</a></li>
				</ul>
			  </div>
		
			  <div class="menu-responsive">
				  <div class="hamburger"><a id="simple-menu" href="#sidr"></a></div>
				  <div class="menu-r" id="sidr">';
				  
					/*$main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'menu-menu-secundario'));
					print drupal_render($main_menu_tree);*/
					$tree = menu_tree('menu-menu-secundario');
					print drupal_render($tree);
				echo '</div>
			  </div>
			</div>
		
			  <div class="l-menu-secundario">';
				echo '<div class="pane-block-3"><h2>' . config_pages_get('configuraci_n_de_sitio', 'field_nombre_del_sitio') . '</h2></div>';
			    echo '<div class="pane-menu-menu-menu-secundario">';
				/*$main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'menu-menu-secundario'));
				
					print drupal_render($main_menu_tree);*/
					
					$tree = menu_tree('menu-menu-secundario');
					print drupal_render($tree);
				echo "</div>";
			   echo '</div>
		
		
			 <div class="l-slider-principal">
				<div class="l-region l-region--slider-principal">';
					$slider_principal = slider_principal($nidFrontal);
					$last = sizeof($slider_principal) - 1;
		
					if ($slider_principal[$last] != "nada") print drupal_render(node_view(node_load($slider_principal[$last])));
					else echo "No hay ningun slider definido.";
					
				echo '</div>
			  </div>
			</div>
		  </header>';

		  echo '<div class="l-page">
  				  <div class="l-main">
					 <div class="l-content-lienzo" role="main">';
					 if ($superpanel == '1') {
						 echo '<div class="superpanel">
								 <div class="columna-izquierda">';
								 if (!empty($array_ci)) {
									foreach ($array_ci as $key => $value) {
										print drupal_render(node_view(node_load($key)));
									}
								}
								else {
									$style = "columna_superpanel";
									$img_uri = config_pages_get('configuraci_n_de_sitio', 'field_img_izquierda_superpanel')["uri"];
									$url_izq = config_pages_get('configuraci_n_de_sitio', 'field_link_izquierdo_superpanel')["url"];
			
									$derivative_uri = image_style_path($style, $img_uri);
									$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
									$new_image_url  = file_create_url($derivative_uri);
									echo "<div class='l-col-img-superpanel-izq'>";
									if ($url_izq == NULL) {
										echo "<img src='" . $new_image_url . "' />";
									}
									else {
										echo "<a href='" . $url_izq . "'><img src='" . $new_image_url . "' /></a>";
									}
			
									echo "</div>";
								}
						echo "</div>";
						echo "<div class='columna-central'>";
								$i = 1;
									foreach ($array_cc as $key => $value) {
										$total = count($array_cc);
										if ($i == 1) {
											echo "<div class='l-cc-col l-cc-first'>";
										}
										else if ($i == $total) {
											echo "<div class='l-cc-col l-cc-last'>";
										}
										else {
											echo "<div class='l-cc-col'>";
										}
										print drupal_render(node_view(node_load($key)));
										echo "</div>";
										$i++;
									 }
						echo "</div>";
						echo "<div class='columna-derecha'>";
							if (!empty($array_cd)) {
								foreach ($array_cd as $key => $value) {
									print drupal_render(node_view(node_load($key)));
								}
							}
							else {
								$style = "columna_superpanel";
								$img_uri = config_pages_get('configuraci_n_de_sitio', 'field_imagen_derecha_superpanel')["uri"];
								$url_der = config_pages_get('configuraci_n_de_sitio', 'field_link_derecha_superpanel')["url"];
		
								$derivative_uri = image_style_path($style, $img_uri);
								$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
								$new_image_url  = file_create_url($derivative_uri);
								echo "<div class='l-col-img-superpanel-der'>";
		
								if ($url_der == NULL) {
									echo "<img src='" . $new_image_url . "' />";
								}
								else {
									echo "<a href='" . $url_der . "'><img src='" . $new_image_url . "' /></a>";
								}
		
								echo "</div>";
							}
						echo '</div>';
					echo "</div>";
					}
					foreach ($pesos as $key => $value) {
						switch ($key) {
							case 'c1':
								if ($c1 == "1") {
		
									if (!empty($array_1col)) {
										$i = 1;
										echo "<div class='l-lienzo-1-col'>";
										foreach ($array_1col as $key => $value) {
											$total = count($array_1col);
		
												if ($i == 1) {
													echo "<div class='l-1-cols-col l-1-cols-first'>";
												}
												else {
													if ($i % 2 == 0) {
														if ($i > 2) {
															echo "<div class='l-1-cols-col l-1-cols-last l-1-cols-row'>";
														} else {
															echo "<div class='l-1-cols-col l-1-cols-last'>";
														}
													}
													else {
														if ($i > 2) {
															echo "<div class='l-1-cols-col l-1-cols-row'>";
														} else {
															echo "<div class='l-1-cols-col'>";
														}
													}
												}
												print drupal_render(node_view(node_load($key)));
												echo "</div>";
												$i++;
		
										}
										echo "</div>";
									}
		
								}
								break;
							case 'c2':
								if ($c2 == "1") {
									if (!empty($array_2col)) {
										$i = 1;
										echo "<div class='l-lienzo-2-cols'>";
										foreach ($array_2col as $key => $value) {
											$total = count($array_2col);
		
												if ($i == 1) {
													echo "<div class='l-2-cols-col l-2-cols-first'>";
												}
												else {
													if ($i % 2 == 0) {
														if ($i > 2) {
															echo "<div class='l-2-cols-col l-2-cols-last l-2-cols-row'>";
														} else {
															echo "<div class='l-2-cols-col l-2-cols-last'>";
														}
													}
													else {
														if ($i > 2) {
															echo "<div class='l-2-cols-col l-2-cols-row'>";
														} else {
															echo "<div class='l-2-cols-col'>";
														}
													}
												}
												print drupal_render(node_view(node_load($key)));
												echo "</div>";
												$i++;
		
										}
										echo "</div>";
									}
								}
								break;
							case 'c3':
								if ($c3 == "1") {
									if (!empty($array_3col)) {								
										$i = 1;
										echo "<div class='l-lienzo-3-cols'>";
		
										foreach ($array_3col as $key => $value) {
											$total = count($array_3col);
												if ($i == 1) {
													echo "<div class='l-3-cols-col l-3-cols-first'>";
												}
												else {
													if ($i % 3 == 0) {
														if ($i > 3) {
															echo "<div class='l-3-cols-col l-3-cols-last l-3-cols-row'>";
														} else {
															echo "<div class='l-3-cols-col l-3-cols-last'>";
														}
													}
													else {
														if ($i > 3) {
															echo "<div class='l-3-cols-col l-3-cols-row'>";
														} else {
															echo "<div class='l-3-cols-col'>";
														}
													}
												}
		
												print drupal_render(node_view(node_load($key)));
												echo "</div>";
												$i++;
		
										}
										echo "</div>";
									}
								}
								break;
							case 'c4':
								if ($c4 == "1") {
									if (!empty($array_4col)) {
									$i = 1;
									echo "<div class='l-lienzo-4-cols'>";
		
									foreach ($array_4col as $key => $value) {
										$total = count($array_4col);
		
											if ($i == 1) {
												echo "<div class='l-4-cols-col l-4-cols-first'>";
											}
		
											else {
												if ($i % 4 == 0) {
													if ($i > 4) {
														echo "<div class='l-4-cols-col l-4-cols-last l-4-cols-row'>";
													} else {
														echo "<div class='l-4-cols-col l-4-cols-last'>";
													}
												}
												else {
													if ($i > 4) {
														echo "<div class='l-4-cols-col l-4-cols-row'>";
													} else {
														echo "<div class='l-4-cols-col'>";
													}
												}
											}
											print drupal_render(node_view(node_load($key)));
											echo "</div>";
											$i++;
		
									}
									echo "</div>";
								}							}
								break;
							case 'c2v':
								if ($c2 == "1") {
									if (!empty($array_2colv)) {
										$j = 1;
										
										echo "<div class='l-lienzo-2-cols-v'>";
										foreach ($array_2colv as $key => $value) {
											$total = count($array_2colv);
												if ($j == 1) {
													echo "<div class='l-2-cols-col-v l-2-cols-v-first'>";
												}
												else {
													if ($j % 2 == 0) {
														if ($j > 2) {
															echo "<div class='l-2-cols-col-v l-2-cols-v-last l-2-cols-v-row'>";
														} else {
															echo "<div class='l-2-cols-col-v l-2-cols-v-last'>";
														}
													}
													else {
														if ($j > 2) {
															echo "<div class='l-2-cols-col-v l-2-cols-v-row'>";
														} else {
															echo "<div class='l-2-cols-col-v'>";
														}
													}
												}
		
												print drupal_render(node_view(node_load($key)));
												echo "</div>";
												$j++;
		
										}
										echo "</div>";
									}
								}
								break;
							case 'c3v':
								if ($c3 == "1") {
									if (!empty($array_3colv)) {
										$j = 1;
										
										echo "<div class='l-lienzo-3-cols-v'>";
										foreach ($array_3colv as $key => $value) {
											$total = count($array_3colv);
												if ($j == 1) {
													echo "<div class='l-3-cols-col-v l-3-cols-v-first'>";
												}
												else {
													if ($j % 3 == 0) {
														if ($j > 3) {
															echo "<div class='l-3-cols-col-v l-3-cols-v-last l-3-cols-v-row'>";
														} else {
															echo "<div class='l-3-cols-col-v l-3-cols-v-last'>";
														}
													}
													else {
														if ($j > 3) {
															echo "<div class='l-3-cols-col-v l-3-cols-v-row'>";
														} else {
															echo "<div class='l-3-cols-col-v'>";
														}
													}
												}
		
												print drupal_render(node_view(node_load($key)));
												echo "</div>";
												$j++;
		
										}
										echo "</div>";
									}
								}
								break;
							case 'c4v':
								if ($c4 == "1") {
									if (!empty($array_4colv)) {
										$j = 1;
										
										echo "<div class='l-lienzo-4-cols-v'>";
										foreach ($array_4colv as $key => $value) {
											$total = count($array_4colv);
												if ($j == 1) {
													echo "<div class='l-4-cols-col-v l-4-cols-v-first'>";
												}
												else {
													if ($j % 4 == 0) {
														if ($j > 4) {
															echo "<div class='l-4-cols-col-v l-4-cols-v-last l-4-cols-v-row'>";
														} else {
															echo "<div class='l-4-cols-col-v l-4-cols-v-last'>";
														}
													}
													else {
														if ($j > 4) {
															echo "<div class='l-4-cols-col-v l-4-cols-v-row'>";
														} else {
															echo "<div class='l-4-cols-col-v'>";
														}
													}
												}
		
												print drupal_render(node_view(node_load($key)));
												echo "</div>";
												$j++;
		
										}
										echo "</div>";
									}
								}
								break;
							default:
								# code...
									echo "no valido";
								break;
						}
					}

		  echo '</div>';
		  
		  echo '<script>
					jQuery.getJSON( "https://aplicaciones.buap.mx/app-buap-common/index.php/API/getLatestNoticias_p", function( data ) {

					var items = [];

					for (i=0;i<=2; i++) {
						/*items.push("<div class=\'l-noticias-buap-col\'><div class=\'content\'><div class=\'noticias-buap\'><div class=\'imagen\'><a href=\'http://www.buap.mx/node/" + data[i].nid + "\'><img src=\'http://www.buap.mx/sites/default/files/styles/noticias_/public/" + data[i].filename + "\' /></a></div><div class=\'titulo\'>" + data[i].title + "</div><div class=\'leer-mas\'><a href=\'http://www.buap.mx/node/" + data[i].nid + "\'>Leer Más</a></div></div></div></div>")*/
						items.push("<div class=\'l-noticias-buap-col\'><div class=\'content\'><div class=\'noticias-buap\'><div class=\'imagen\'><a href=\'https://boletin.buap.mx/?q=node/" + data[i].nid + "\'><img src=\'" + data[i].filename + "\' /></a></div><div class=\'titulo\'>" + data[i].title + "</div><div class=\'leer-mas\'><a href=\'https://boletin.buap.mx/?q=node/" + data[i].nid + "\'>Leer Más</a></div></div></div></div>")
					}

					jQuery( "<div/>", {
						"class": "l-noticias-buap-cols",
						html: items.join( "" )
					}).appendTo( ".l-noticias-buap" );
					});
					</script>';
					echo '<div class="l-noticias-buap">
						<p class="titulo">Entérate de lo que sucede en la BUAP</p>

					</div>';
		echo "</div>";
	echo "</div>";
	
	footer_plantilla();
  echo "</div>";
  echo "</div>";
}
