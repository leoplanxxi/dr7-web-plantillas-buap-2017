name = interior
description = Plantilla 1 BUAP - Interior.
preview = preview.png
template = interior-layout

; ========================================
; Stylesheets
; ========================================
stylesheets[all][] = css/layouts/home/home.layout.css
stylesheets[all][] = css/layouts/home/home.layout.no-query.css
stylesheets[all][] = css/layouts/home/level-home.layout.css


; ========================================
; Scripts
; ========================================
scripts[] = js/web-contacto-rector.behaviors.js
scripts[] = js/web-contacto-rector.home.custom.js

; ========================================
; Regions
; ========================================
regions[menu-buap] = Menu BUAP
regions[menu-secundario] = Menu Secundario
regions[content] = Contenido
regions[footer] = Footer
