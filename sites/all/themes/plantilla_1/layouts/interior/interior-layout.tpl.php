<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['branding']: Items for the branding region.
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see omega_preprocess_page()
 */

global $base_url;
$id_analytics = config_pages_get("configuraci_n_de_sitio", "field_id_analytics");
require __DIR__ . '/../common/main.php';
?>
<script>
    <?php
    print config_pages_get('scripts_y_css_personalizados', 'field_javascript');
    ?>
</script>

<!-- ESTILOS PERSONALIZADOS -->
<style>
    <?php
        print config_pages_get('scripts_y_css_personalizados', 'field_css');
    ?>
</style>
<script src="<?php echo $base_url; ?>/sites/all/libraries/sidr/jquery.sidr.min.js"></script>
<script async="" src="//www.google-analytics.com/analytics.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/sites/all/libraries/sidr/stylesheets/jquery.sidr.bare.css" media="all">
<script>
    jQuery(window).load(function() {
        jQuery('#simple-menu').sidr({
            side: 'right'
        });
    });
</script>
<script>
    document.title = "<?php echo drupal_get_title(); ?> | <?php print config_pages_get('configuraci_n_de_sitio', 'field_nombre_del_sitio'); ?>";
</script>

<?php if ($id_analytics != NULL): ?>
<script>
    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
    ga('create', '<?php echo $id_analytics; ?>', 'auto');
    ga('send', 'pageview');
</script>
<?php endif; ?>
<?php
global $base_url;

//echo $base_url.'/sites/all/themes/plantilla_1/layouts/common/impl/common.php';
?>
<style>
    .hamburger {
        width: 65px;
        height: 65px;

    }

    .hamburger a {
        background: url(<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/menu_responsive_blanco.png);
        height: 65px;
        width: 65px;
        background-repeat: no-repeat;
        background-position: center;
    }
    .hamburger a:hover {
        background: url(<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/menu_responsive_azul.png);
    }
</style>
<style>
    <?php if ($mostrar_overlay != '1'): ?>
    .peso {
        display: none;
    }
    <?php endif; ?>
</style>
<div class='container'>
    <div class="l-page">
        <header class="l-header" role="banner">
            <div class="l-branding">
                <div class="l-menu">
                    <div class="l-region--menu-buap">
                        <?php #print render($page['menu-buap']); ?>
                        <div class="header-logo-buap">
                            <a href="http://buap.mx"><img src="<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/escudo_blanco.png" /></a>
                        </div>

                        <div class="header-redes">
                            <a href="https://www.facebook.com/BUAPoficial/"><img src="<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/facebook.png"></a>
                            <a href="https://twitter.com/buapoficial?lang=en"><img src="<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/twitter.png"></a>
                            <a href="https://www.instagram.com/buapoficial/"><img src="<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/instagram.png"></a>
                            <a href="https://www.youtube.com/user/ibuap"><img src="<?php echo $base_url; ?>/sites/all/themes/plantilla_1/images/youtube.png"></a>
                            <?php $form = drupal_get_form('search_block_form', TRUE); ?>
                            <?php print render($form); ?>
                        </div>

                        <ul class="menu"><li class="first leaf"><a href="http://www.autoservicios.buap.mx/" target="_blank">Autoservicios</a></li>
                            <li class="last leaf"><a href="http://www.correo.buap.mx/" target="_blank">Correo BUAP</a></li>
                        </ul>
                    </div>

                    <div class="menu-responsive">
                        <div class="hamburger"><a id="simple-menu" href="#sidr"></a></div>
                        <div class="menu-r" id="sidr"><?php
                            $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'menu-menu-secundario'));
                            print drupal_render($main_menu_tree);
                            ?></div>
                    </div>
                </div>

                <div class='l-menu-secundario'>

		<?php


                print render($page['menu-secundario']);

                ?>

	            </div>
            </div>
        </header>

        <div class="l-main">

            <div class="l-content" role="main">
            
            <?php

/*
 * BREADCRUMBS
 */
echo '<div class="l-content-breadcrumbs">';
$res = db_query('SELECT * FROM {menu_links} WHERE link_path = \''.current_path().'\' AND menu_name = \'menu-menu-secundario\'');
                // Si es elemento del menú secundario
                if($res->rowCount() > 0){

                    $record = $res->fetchAssoc();
                    $bc = $record['link_title'];

                    // Si su elemento padre es él mismo
                    if($record['depth'] == 1){
                        $bc = '<br/><a href="?q=">Inicio</a> > ' . $bc;
                        print $bc;
                    }

                    //Si su elemento padre es diferente a él mismo
                    else {
                        $x = (int)$record['depth']-1;
                        $plid = $record['plid'];
                        for($i = 0; $i < (int)$record['depth']; $i++) {
                            $res = db_query('SELECT * FROM {menu_links} WHERE mlid = \'' . $plid . '\'');
                            $pl = $res->fetchAssoc();
                            $plid = $pl['plid'];
                            $bc = '<a href="'. $pl['link_path'] .'">'.$pl['link_title'].'</a> > ' . $bc;

                        }

                        print '<br/><a href="?q=node">Inicio</a> '.$bc;
                    }

                }

                // Si es elemento de un menú
                else {
                    $res = db_query('SELECT entity_id FROM {field_data_field_link} WHERE field_link_url = \'' . current_path() . '\' AND bundle = \'menu\'');
                    if($res->rowCount() > 0){
                        $record = $res->fetchAssoc();
                        $r2 = db_query('SELECT title FROM {node} WHERE nid = \''.$record['entity_id'].'\'');
                        $rec = $r2->fetchAssoc();
                        $ruta = drupal_get_path_alias("node/".$record['entity_id']);
                        print '<a href="?q=">Inicio</a> > <a href="?q=node/'. $ruta .'">'.$rec['title'].'</a> > '.drupal_get_title();

                    }
                    else{
                        print '<a href="?q=">Inicio</a> > '.drupal_get_title();
                    }

                }
echo '</div>';

//echo current_path();

print render($page['content']);
// echo menu_izquierdo();
?>
	        </div>

        </div>



    </div>


    <?php footer_plantilla(); ?>
</div>
</div>
