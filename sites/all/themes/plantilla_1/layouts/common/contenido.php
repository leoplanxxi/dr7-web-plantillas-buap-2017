<?php
/*
 EXPERIMENTAL

 $nidPagFrontal recupera los recursos para una página frontal, según su NID. En el caso de la página frontal inicial, el $nidPagFrontal = 0.
*/
function slider_principal($nidPagFrontal) {
    $array_slider_principal = array();

	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'slider_principal')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_slider_principal_slider', 'value', '1', '=')
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();

    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);
        foreach ($html_items as $html) {
            $nid              = $html->nid;
            array_push($array_slider_principal, $nid);
        }
    }
    else {
        array_push($array_slider_principal, "nada");
    }

    return $array_slider_principal;

}

function cuatro_columnas_var($nidPagFrontal) {
	$array_html        = array();
	$array_btn_link    = array();

	# Bloque HTML
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '4', '=')->fieldCondition('field_alto_variable_html', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

	# Boton + Link
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bot_n_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_boton_link', 'value', '4', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_boton_link["und"][0]["value"];
            $array_btn_link[$nid] = $orden;
        }
    }

	$array_4colv = $array_html + $array_btn_link;

    asort($array_4colv);
    return $array_4colv;

}


function tres_columnas_var($nidPagFrontal) {
	$array_html        = array();
	$array_btn_link    = array();

	# Bloque HTML
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '3', '=')->fieldCondition('field_alto_variable_html', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

	# Boton + Link
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bot_n_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_boton_link', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_boton_link["und"][0]["value"];
            $array_btn_link[$nid] = $orden;
        }
    }

	$array_3colv = $array_html + $array_btn_link;

    asort($array_3colv);
    return $array_3colv;

}

function dos_columnas_var($nidPagFrontal) {
	$array_html        = array();
	$array_btn_link    = array();

	# Bloque HTML
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '2', '=')->fieldCondition('field_alto_variable_html', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

	# Boton + Link
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bot_n_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_boton_link', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_boton_link["und"][0]["value"];
            $array_btn_link[$nid] = $orden;
        }
    }

	$array_2colv = $array_html + $array_btn_link;

    asort($array_2colv);
    return $array_2colv;

}


function cuatro_columnas($nidPagFrontal)
{
    $array_menu        = array();
    $array_galeria     = array();
    $array_video       = array();
    $array_imagen_link = array();
    $array_imagen      = array();
	  $array_html        = array();

    //Menus de 4 columnas
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'menu')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_menu', 'value', '4', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $menu_items_nids = array_keys($result['node']);
        $menu_items      = entity_load('node', $menu_items_nids);

        foreach ($menu_items as $menu) {
            $nid              = $menu->nid;
            $orden            = $menu->field_orden_menu["und"][0]["value"];
            $array_menu[$nid] = $orden;
        }
    }

    //Imagen
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen', 'value', '4', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_items_nids = array_keys($result['node']);
        $imagen_items      = entity_load('node', $imagen_items_nids);

        foreach ($imagen_items as $imagen) {
            $nid                = $imagen->nid;
            $orden              = $imagen->field_orden_imagen["und"][0]["value"];
            $array_imagen[$nid] = $orden;
        }
    }

		//HTML
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '4', '=')->fieldCondition('field_alto_variable_html', 'value', '0', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

    //Imagen + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen_con_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen_link', 'value', '4', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_link_items_nids = array_keys($result['node']);
        $imagen_link_items      = entity_load('node', $imagen_link_items_nids);

        foreach ($imagen_link_items as $imagen_link) {
            $nid                     = $imagen_link->nid;
            $orden                   = $imagen_link->field_orden_imagen_link["und"][0]["value"];
            $array_imagen_link[$nid] = $orden;
        }
    }

    //Galeria
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'galer_a')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas', 'value', '4', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $galeria_items_nids = array_keys($result['node']);
        $galeria_items      = entity_load('node', $galeria_items_nids);

        foreach ($galeria_items as $galeria) {
            $nid                 = $galeria->nid;
            $orden               = $galeria->field_orden_galeria["und"][0]["value"];
            $array_galeria[$nid] = $orden;
        }
    }

    //Video
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'video_youtube')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_video', 'value', '4', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $video_items_nids = array_keys($result['node']);
        $video_items      = entity_load('node', $video_items_nids);

        foreach ($video_items as $video) {
            $nid               = $video->nid;
            $orden             = $video->field_orden_video["und"][0]["value"];
            $array_video[$nid] = $orden;
        }
    }

    $array_4col = $array_menu + $array_galeria + $array_video + $array_imagen_link + $array_html + $array_imagen;

    asort($array_4col);
    return $array_4col;

}

function tres_columnas($nidPagFrontal)
{
    $array_menu        = array();
    $array_texto_link  = array();
    $array_galeria     = array();
    $array_video       = array();
    $array_imagen_link = array();
    $array_imagen      = array();
		$array_html 			 = array();

    //Menus de 3 columnas
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'menu')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_menu', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $menu_items_nids = array_keys($result['node']);
        $menu_items      = entity_load('node', $menu_items_nids);

        foreach ($menu_items as $menu) {
            $nid              = $menu->nid;
            $orden            = $menu->field_orden_menu["und"][0]["value"];
            $array_menu[$nid] = $orden;
        }
    }

		//HTML
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '3', '=')->fieldCondition('field_alto_variable_html', 'value', '0', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

    //Imagen
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_items_nids = array_keys($result['node']);
        $imagen_items      = entity_load('node', $imagen_items_nids);

        foreach ($imagen_items as $imagen) {
            $nid                = $imagen->nid;
            $orden              = $imagen->field_orden_imagen["und"][0]["value"];
            $array_imagen[$nid] = $orden;
        }
    }

    //Texto HTML + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'texto_html_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_texto_link', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $texto_link_items_nids = array_keys($result['node']);
        $texto_link_items      = entity_load('node', $texto_link_items_nids);

        foreach ($texto_link_items as $texto_link) {
            $nid                    = $texto_link->nid;
            $orden                  = $texto_link->field_orden_texto_link["und"][0]["value"];
            $array_texto_link[$nid] = $orden;
        }
    }

    //Imagen + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen_con_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen_link', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_link_items_nids = array_keys($result['node']);
        $imagen_link_items      = entity_load('node', $imagen_link_items_nids);

        foreach ($imagen_link_items as $imagen_link) {
            $nid                     = $imagen_link->nid;
            $orden                   = $imagen_link->field_orden_imagen_link["und"][0]["value"];
            $array_imagen_link[$nid] = $orden;
        }
    }

    //Galeria
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'galer_a')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $galeria_items_nids = array_keys($result['node']);
        $galeria_items      = entity_load('node', $galeria_items_nids);

        foreach ($galeria_items as $galeria) {
            $nid                 = $galeria->nid;
            $orden               = $galeria->field_orden_galeria["und"][0]["value"];
            $array_galeria[$nid] = $orden;
        }
    }

    //Video
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'video_youtube')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_video', 'value', '3', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $video_items_nids = array_keys($result['node']);
        $video_items      = entity_load('node', $video_items_nids);

        foreach ($video_items as $video) {
            $nid               = $video->nid;
            $orden             = $video->field_orden_video["und"][0]["value"];
            $array_video[$nid] = $orden;
        }
    }

    $array_3col = $array_menu + $array_texto_link + $array_galeria + $array_video + $array_imagen_link + $array_imagen + $array_html;

    asort($array_3col);
    return $array_3col;
}

function dos_columnas($nidPagFrontal)
{
    $array_menu       = array();
    $array_texto_link = array();
    $array_galeria    = array();
    $array_video      = array();
    $array_imagen     = array();
    $array_rd         = array();
    $array_html       = array();
    $array_mapa       = array();
	$array_imagen_link = array();

	//Imagen + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen_con_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen_link', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_link_items_nids = array_keys($result['node']);
        $imagen_link_items      = entity_load('node', $imagen_link_items_nids);

        foreach ($imagen_link_items as $imagen_link) {
            $nid                     = $imagen_link->nid;
            $orden                   = $imagen_link->field_orden_imagen_link["und"][0]["value"];
            $array_imagen_link[$nid] = $orden;
        }
    }

    // Mapa a 2 columnas
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'mapa')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_mapa', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $mapa_items_nids = array_keys($result['node']);
        $mapa_items      = entity_load('node', $mapa_items_nids);

        foreach ($mapa_items as $mapa) {
            $nid              = $mapa->nid;
            $orden            = $mapa->field_orden_mapa["und"][0]["value"];
            $array_mapa[$nid] = $orden;
        }
    }

    //Menus de 2 columnas
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'menu')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_menu', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $menu_items_nids = array_keys($result['node']);
        $menu_items      = entity_load('node', $menu_items_nids);

        foreach ($menu_items as $menu) {
            $nid              = $menu->nid;
            $orden            = $menu->field_orden_menu["und"][0]["value"];
            $array_menu[$nid] = $orden;
        }
    }
    //HTML
    $query = new EntityFieldQuery();
    //$query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '2', '=')->fieldCondition('field_alto_variable_html', 'value', '0', '=')->fieldCondition('field_p_gina_frontal_html','tid',$nidPagFrontal,'=')
        $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '2', '=')->fieldCondition('field_alto_variable_html', 'value', '0', '=')->propertyCondition('promote', 1)
        // Run the query as user 1.
            ->addMetaData('account', user_load(1));
    
    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);
        // Filtrado por página principal

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }
    
    // Imagen
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_items_nids = array_keys($result['node']);
        $imagen_items      = entity_load('node', $imagen_items_nids);

        foreach ($imagen_items as $imagen) {
            $nid                = $imagen->nid;
            $orden              = $imagen->field_orden_imagen["und"][0]["value"];
            $array_imagen[$nid] = $orden;
        }
    }

    //Texto HTML + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'texto_html_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_texto_link', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $texto_link_items_nids = array_keys($result['node']);
        $texto_link_items      = entity_load('node', $texto_link_items_nids);

        foreach ($texto_link_items as $texto_link) {
            $nid                    = $texto_link->nid;
            $orden                  = $texto_link->field_orden_texto_link["und"][0]["value"];
            $array_texto_link[$nid] = $orden;
        }
    }

    //Galeria
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'galer_a')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $galeria_items_nids = array_keys($result['node']);
        $galeria_items      = entity_load('node', $galeria_items_nids);

        foreach ($galeria_items as $galeria) {
            $nid                 = $galeria->nid;
            $orden               = $galeria->field_orden_galeria["und"][0]["value"];
            $array_galeria[$nid] = $orden;
        }
    }

    //Video
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'video_youtube')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_video', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $video_items_nids = array_keys($result['node']);
        $video_items      = entity_load('node', $video_items_nids);

        foreach ($video_items as $video) {
            $nid               = $video->nid;
            $orden             = $video->field_orden_video["und"][0]["value"];
            $array_video[$nid] = $orden;
        }
    }

    //RD
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'recursos_din_micos')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_rd', 'value', '2', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $rd_items_nids = array_keys($result['node']);
        $rd_items      = entity_load('node', $rd_items_nids);

        foreach ($rd_items as $rd) {
            $nid            = $rd->nid;
            $orden          = $rd->field_orden_rd["und"][0]["value"];
            $array_rd[$nid] = $orden;
        }
    }

    $array_2col = $array_menu + $array_texto_link + $array_galeria + $array_video + $array_rd + $array_html + $array_imagen + $array_mapa + $array_imagen_link;

    asort($array_2col);
    return $array_2col;
}

function una_columna($nidPagFrontal)
{
    $array_video = array();
    $array_mapa      = array();
    $array_carrousel = array();
    $array_rd         = array();
    $array_html      = array();
    $array_lienzo	 = array();
    $array_btn_link   = array();

    //Carrousel
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'carrousel_')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_carrousel', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $carrousel_items_nids = array_keys($result['node']);
        $carrousel_items      = entity_load('node', $carrousel_items_nids);

        foreach ($carrousel_items as $carrousel) {
            $nid                   = $carrousel->nid;
            $orden                 = $carrousel->field_orden_carrousel["und"][0]["value"];
            $array_carrousel[$nid] = $orden;
        }
    }

    //Video YT
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'video_youtube')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_video', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $video_items_nids = array_keys($result['node']);
        $video_items      = entity_load('node', $video_items_nids);

        foreach ($video_items as $video) {
            $nid                   = $video->nid;
            $orden                 = $video->field_orden_video["und"][0]["value"];
            $array_video[$nid] = $orden;
        }
    }

	//Carrousel
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'lienzo')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_lienzo', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $lienzo_items_nids = array_keys($result['node']);
        $lienzo_items      = entity_load('node', $lienzo_items_nids);

        foreach ($lienzo_items as $lienzo) {
            $nid                   = $lienzo->nid;
            $orden                 = $lienzo->field_orden_lienzo["und"][0]["value"];
            $array_lienzo[$nid] = $orden;
        }
    }

    //Mapa
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'mapa')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_mapa', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $mapa_items_nids = array_keys($result['node']);
        $mapa_items      = entity_load('node', $mapa_items_nids);

        foreach ($mapa_items as $mapa) {
            $nid              = $mapa->nid;
            $orden            = $mapa->field_orden_mapa["und"][0]["value"];
            $array_mapa[$nid] = $orden;
        }
    }

    # Bloque HTML
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', '1', '=')->fieldCondition('field_alto_variable_html', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

    //Recursos Dinamicos
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'recursos_din_micos')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_rd', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $rd_items_nids = array_keys($result['node']);
        $rd_items      = entity_load('node', $rd_items_nids);

        foreach ($rd_items as $rd) {
            $nid            = $rd->nid;
            $orden          = $rd->field_orden_rd["und"][0]["value"];
            $array_rd[$nid] = $orden;
        }
    }

    # Boton + Link
	$query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bot_n_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_boton_link', 'value', '1', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_boton_link["und"][0]["value"];
            $array_btn_link[$nid] = $orden;
        }
    }


    $array_1col = $array_mapa + $array_carrousel + $array_lienzo + $array_rd + $array_html + $array_video + $array_btn_link;

    asort($array_1col);
    return $array_1col;
}

function columna_central_superpanel($nidPagFrontal)
{
    $array_menu        = array();
    $array_texto_link  = array();
    $array_galeria     = array();
    $array_video       = array();
    $array_imagen_link = array();
    $array_html 			 = array();
    $array_imagen      = array();

    //Menus de 3 columnas
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'menu')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_menu', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $menu_items_nids = array_keys($result['node']);
        $menu_items      = entity_load('node', $menu_items_nids);

        foreach ($menu_items as $menu) {
            $nid              = $menu->nid;
            $orden            = $menu->field_orden_menu["und"][0]["value"];
            $array_menu[$nid] = $orden;
        }
    }

		//HTML
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'bloque_html')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_html', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $html_items_nids = array_keys($result['node']);
        $html_items      = entity_load('node', $html_items_nids);

        foreach ($html_items as $html) {
            $nid              = $html->nid;
            $orden            = $html->field_orden_html["und"][0]["value"];
            $array_html[$nid] = $orden;
        }
    }

    // Imagen
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_items_nids = array_keys($result['node']);
        $imagen_items      = entity_load('node', $imagen_items_nids);

        foreach ($imagen_items as $imagen) {
            $nid                = $imagen->nid;
            $orden              = $imagen->field_orden_imagen["und"][0]["value"];
            $array_imagen[$nid] = $orden;
        }
    }

    //Texto HTML + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'texto_html_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_texto_link', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $texto_link_items_nids = array_keys($result['node']);
        $texto_link_items      = entity_load('node', $texto_link_items_nids);

        foreach ($texto_link_items as $texto_link) {
            $nid                    = $texto_link->nid;
            $orden                  = $texto_link->field_orden_texto_link["und"][0]["value"];
            $array_texto_link[$nid] = $orden;
        }
    }

    //Imagen + Link
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'imagen_con_link')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_imagen_link', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $imagen_link_items_nids = array_keys($result['node']);
        $imagen_link_items      = entity_load('node', $imagen_link_items_nids);

        foreach ($imagen_link_items as $imagen_link) {
            $nid                     = $imagen_link->nid;
            $orden                   = $imagen_link->field_orden_imagen_link["und"][0]["value"];
            $array_imagen_link[$nid] = $orden;
        }
    }

    //Galeria
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'galer_a')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $galeria_items_nids = array_keys($result['node']);
        $galeria_items      = entity_load('node', $galeria_items_nids);

        foreach ($galeria_items as $galeria) {
            $nid                 = $galeria->nid;
            $orden               = $galeria->field_orden_galeria["und"][0]["value"];
            $array_galeria[$nid] = $orden;
        }
    }

    //Video
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'video_youtube')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_video', 'value', 'Columna Central', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $video_items_nids = array_keys($result['node']);
        $video_items      = entity_load('node', $video_items_nids);

        foreach ($video_items as $video) {
            $nid               = $video->nid;
            $orden             = $video->field_orden_video["und"][0]["value"];
            $array_video[$nid] = $orden;
        }
    }

    $array_cc = $array_menu + $array_texto_link + $array_galeria + $array_video + $array_imagen_link + $array_html + $array_imagen;

    asort($array_cc);
    return $array_cc;
}

function columna_izquierda_superpanel($nidPagFrontal)
{
    $array_menu = array();

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'menu')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_menu', 'value', 'Columna Izquierda', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $menu_items_nids = array_keys($result['node']);
        $menu_items      = entity_load('node', $menu_items_nids);

        foreach ($menu_items as $menu) {
            $nid              = $menu->nid;
            $orden            = $menu->field_orden_menu["und"][0]["value"];
            $array_menu[$nid] = $orden;
        }
    }
    return $array_menu;
}

function columna_derecha_superpanel($nidPagFrontal)
{
    $array_menu = array();

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'menu')->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_columnas_menu', 'value', 'Columna Derecha', '=')->propertyCondition('promote', 1)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $menu_items_nids = array_keys($result['node']);
        $menu_items      = entity_load('node', $menu_items_nids);

        foreach ($menu_items as $menu) {
            $nid              = $menu->nid;
            $orden            = $menu->field_orden_menu["und"][0]["value"];
            $array_menu[$nid] = $orden;
        }
    }
    return $array_menu;
}

function noticias_buap()
{
    $array_noticias = array();

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'noticias_buap')->propertyCondition('status', NODE_PUBLISHED)->propertyOrderBy('changed', 'DESC')->range(0, 3)
    // Run the query as user 1.
        ->addMetaData('account', user_load(1));

    $result = $query->execute();
    if (isset($result['node'])) {
        $noticias_buap_items_nids = array_keys($result['node']);
        $noticias_buap_items      = entity_load('node', $noticias_buap_items_nids);

        $i = 0;

        foreach ($noticias_buap_items as $noticia) {
            $nid                = $noticia->nid;
            $array_noticias[$i] = $nid;
            $i++;
        }
    }
    return $array_noticias;
}
?>
