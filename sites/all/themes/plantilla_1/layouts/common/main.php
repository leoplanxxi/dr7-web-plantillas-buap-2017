<?php
function header_plantilla()
{
	
}

function footer_plantilla() {
	echo '<footer class="l-footer" role="contentinfo">
	<div class="l-region--footer">
    	<div class="footer-logo-buap">
			<img src="sites/all/themes/plantilla_1/images/logo_buap.png">
		</div>
		<div class="footer-info-buap">
		<p><strong>Benemérita Universidad Autónoma de Puebla</strong><br>
		4 Sur 104 Centro Histórico C.P. 72000<br>
		Teléfono +52 (222) 229 55 00</p>
		</div>
		<ul class="menu"><li class="first leaf"><a href="https://www.buap.mx/calendario-escolar" target="_blank">Calendario Escolar</a></li>
			<li class="leaf"><a href="http://www.dci.buap.mx/?q=content/identidad-gr%C3%A1fica" target="_blank">Identidad Gráfica</a></li>
			<li class="leaf"><a href="https://www.buap.mx/directorio-telefonico" target="_blank">Directorio</a></li>
			<li class="leaf"><a href="http://www.comunicacion.buap.mx/" target="_blank">Prensa</a></li>
			<li class="last leaf"><a href="https://www.buap.mx/privacidad" target="_blank">Aviso de privacidad</a></li>
			<li class="last leaf"><a href="http://www.transparencia.buap.mx/" target="_blank">Transparencia y Acceso a la Información</a></li>
			<li class="last leaf"><a href="https://consultapublicamx.inai.org.mx/vut-web/?idSujetoObigadoParametro=4479&idEntidadParametro=21&idSectorParametro=24" target="_blank">Obligaciones de Transparencia</a></li>
		</ul>
		<div class="footer-info-dep">
			<strong>' . config_pages_get('configuraci_n_de_sitio', 'field_nombre_del_sitio') . '</strong>'
			. config_pages_get('configuraci_n_de_sitio', 'field_direcci_n_sitio') . 
		'</div>
	</div>
  </footer>';
}