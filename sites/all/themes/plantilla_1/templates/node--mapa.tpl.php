<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
global $base_url;
$var['cols'] = field_view_field("node", $node, 'field_columnas_mapa')["#object"]->field_columnas_mapa["und"][0]["value"];

$var['peso'] = field_view_field("node", $node, 'field_orden_mapa')["#object"]->field_orden_mapa["und"][0]["value"];

if ($var["cols"] == "1") {
    $attr["cols"] = "mapa-1-col";
}
else if ($var["cols"] == "2") {
    $attr["cols"] = "mapa-2-cols";
}
else if ($var["cols"] == "3") {
    $attr["cols"] = "mapa-3-cols";
}



?>

<!--<script async defer src="http://maps.google.cn/maps/api/js?key=AIzaSyADVQf-Il4q8fqdP0jutRSl3UuR8n7HJ50&v=3&sensor=false&callback=iniciar"  type="text/javascript"></script>-->

<script type="text/javascript">

    var nid = "<?php echo $node->nid; ?>";
    function iniciar(lat, lon, mensaje) {
        const nombre = 'map_canvas_'+ nid;
        //console.log(nombre);

        var html = document.getElementById(nombre);

        mapas[nid] = new L.Map(html).setView([lat, lon], 17);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapas[nid]);
        L.marker([lat, lon]).addTo(mapas[nid]).bindPopup(mensaje).openPopup();
    }

    function marcador(lati,lon,mensaje,nid) {
        var nombre = 'map_canvas_'+ nid;
        //console.log(nombre);

        mapas[nid].remove();

        var html = document.getElementById(nombre);

        mapas[nid] = new L.Map(html).setView([lati, lon], 17);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapas[nid]);
        L.marker([lati, lon]).addTo(mapas[nid]).bindPopup(mensaje).openPopup();
    }
</script>
<style>
    .zerowidth {
        width: 0px !important;
    }

    .nodisplay {
        display: none;
    }

    .mapcanvas{
        height: 385px;
    }
</style>
<script>
    jQuery(document).ready(function() {
        jQuery("#node-<?php print $node->nid; ?> #toggler-map").click(function(){
            jQuery("#node-<?php print $node->nid; ?> #columnas").toggleClass("zerowidth");
            jQuery("#node-<?php print $node->nid; ?> #columnas ul").toggleClass("nodisplay");
        });
        jQuery("#node-<?php print $node->nid; ?> .togglerMap").click(function(){
            jQuery("#node-<?php print $node->nid; ?> #columnas").toggleClass("zerowidth");
            jQuery("#node-<?php print $node->nid; ?> #columnas ul").toggleClass("nodisplay");
        });
    });

</script>
<?php

$itemsN = field_get_items('node', $node, 'field_nombre_ubicacion');
$itemsD = field_get_items('node', $node, 'field_direccion_ubicacion');
$itemsU = field_get_items('node', $node, 'field_ubicacion');
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>

    <?php print $user_picture; ?>

    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted): ?>
        <div class="submitted">
      <?php print $submitted; ?>
    </div>
    <?php endif; ?>

    <div class="content" <?php print $content_attributes; ?>>
        <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        ?>
		<?php if ($attr["cols"] == "mapa-1-col"): ?>
			<h2 style='text-align: center;'><?php echo $title; ?></h2>

		<?php endif; ?>
        <div class="<?php echo $attr["cols"]; ?>">
            <?php if (user_is_logged_in()) : ?>
                <div class="peso"><?php echo $var["peso"]?></div>
            <?php endif; ?>
            <div id="columnas">
                <div class="contenido-columnas"><?php
                    if ($attr["cols"] != "mapa-1-col") {
                        echo "<div class='titulo'>" . $title . "</div>";
					}
                    ?>
                    <ul>
                        <?php
                        if ($attr["cols"] == "mapa-1-col") {
                            $i = 0;
                            foreach ($itemsN as $item)
                            {
                                $latit=$node->field_ubicacion['und'][$i]['lat'];
                                $longi=$node->field_ubicacion['und'][$i]['lng'];
                                echo '<div id="ctexto_'.$node->nid.'"><li>	<a onClick="javascript:marcador('.$latit.','.$longi.', \''.$node->field_nombre_ubicacion['und'][$i]['value'].'\','.$node->nid.');"><img src='. $base_url . '/sites/all/themes/plantilla_1/images/marker.png height="75" width="75" align="left"><p>'
                                    . $node->field_nombre_ubicacion['und'][$i]['value'].'</a><br>';
                                echo $node->field_direccion_ubicacion['und'][$i]['value'].'</li><br><br></p>';
                                $i++;
                            }
                        }
                        else {
                            $i = 0;

                            foreach ($itemsN as $item)
                            {
                                $latit=$node->field_ubicacion['und'][$i]['lat'];
                                $longi=$node->field_ubicacion['und'][$i]['lng'];
                                echo '<div id="ctexto_'.$node->nid.'"><li><p><a onClick="javascript:marcador('.$latit.','.$longi.', \''.$node->field_nombre_ubicacion['und'][$i]['value'].'\','.$node->nid.');" class="togglerMap">'
                                    . $node->field_nombre_ubicacion['und'][$i]['value'].'</a></p></li>';
                                $i++;
                            }
                        }
                        ?>
                    </ul>
                </div>
                <?php if ($attr["cols"] != "mapa-1-col"): ?>
                    <div class="tab">
                        <a href="javascript:void(0);" id="toggler-map">></a>
                    </div>
                <?php endif; ?>
            </div>
            <div id="cmapa">
                <div class="mapcanvas" id="map_canvas_<?php echo $node->nid; ?>"></div>
            </div>
            <script>
                iniciar(<?php echo $node->field_ubicacion['und'][0]['lat']; ?>, <?php echo $node->field_ubicacion['und'][0]['lng']; ?>, '<?php echo $node->field_nombre_ubicacion['und'][0]['value']; ?>');
            </script>

        </div>
    </div>


</div>
