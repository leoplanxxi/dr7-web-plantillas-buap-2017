<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 global $base_url;
?>
<?php

//$items = field_get_items('node', $node, 'field_imagenes');
$plantillas = drupal_get_path('theme',$GLOBALS['theme']);

/*Sección Principal Directorio */


$nombre_directorio = field_view_field("node", $node, 'field_nombre_dir_uac')["#object"]->field_nombre_dir_uac["und"][0]["value"];
$correo_directorio = field_view_field("node", $node, 'field_correo_dir_uac')["#object"]->field_correo_dir_uac["und"][0]["value"];
$telefono_directorio = field_view_field("node", $node, 'field_tel_fono_dir_uac')["#object"]->field_tel_fono_dir_uac["und"][0]["value"];
$ubicacion_directorio = field_view_field("node", $node, 'field_ubicaci_n_dir_uac')["#object"]->field_ubicaci_n_dir_uac["und"][0]["value"];
$descripcion_directorio = field_view_field("node", $node, 'field_descripci_n_dir_uac')["#object"]->field_descripci_n_dir_uac["und"][0]["value"];

$items_pila = field_view_field("node", $node, 'field_pila_dir_uac')["#object"]->field_pila_dir_uac["und"];

$titulo_secc_coord = field_view_field("node", $node, 'field_t_tulo_secc_coord_dir_uac')["#object"]->field_t_tulo_secc_coord_dir_uac["und"][0]["value"];
$items_coord = field_get_items('node', $node, 'field_secc_coord_dir_uac');

$titulo_secc_body = field_view_field("node", $node, 'field_titulo_cuerpo_dir_uac')["#object"]->field_titulo_cuerpo_dir_uac["und"][0]["value"];
?>
<style>

  
    
    ?>
</style>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
	  
    ?>
    <div class="contenido-directorio-uac">
      <div class="top-directorio-uac">
        <div class="stripe-top"></div>
        <div class="stripe-middle"></div>
        <p>&nbsp;</p>
        <div class="top-directorio-uac-izquierda">
          <div class="foto">
            <?php 
              $imagen = field_view_field("node", $node, 'field_foto_dir_uac')["#object"]->field_foto_dir_uac["und"][0]["filename"];
              $img_uri = field_view_field("node", $node, 'field_foto_dir_uac')["#object"]->field_foto_dir_uac["und"][0]["uri"];

              $style="directorio_uac_1";
              $derivative_uri = image_style_path($style, $img_uri);
              $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
              $new_image_url  = file_create_url($derivative_uri);

              echo "<img src='" .  $new_image_url . "'>";
            ?>
          </div>
        </div>
        <div class="top-directorio-uac-derecha">
          <div class="titulo"><h1><?php print $title; ?></h1></div>
          <div class="nombre"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/nombre.png";?>"><?php echo $nombre_directorio; ?></div>
          <div class="correo"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/correo.png";?>"><?php echo $correo_directorio; ?></div>
          <div class="telefono"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/telefono.png";?>"><?php echo $telefono_directorio; ?></div>
          <div class="ubicacion"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/ubicacion.png";?>"><?php echo $ubicacion_directorio; ?></div>
          <p>&nbsp;</p>
          <div class="descripcion"> <?php echo $descripcion_directorio; ?></div>
        </div>
        
    
       
      </div>

      <?php if ($items_pila) :?>
      <div class="directorio-pila">
      <?php /* SOLO ESTA IMPLEMENTADO PARA 3 COLS */ ?>
      <?php
      $i = 1;
      foreach ($items_pila as $link) {
        echo "<div class='directorio-pila-" . $i . "'>";
        $alias = str_replace($base_url,"", $link);
        $alias = str_replace("/?q=", "", $alias);
        $alias = $alias["url"];
        $path = drupal_lookup_path("source", $alias);

        if ($path != false) {
          $node = menu_get_object("node", 1, $path);
          $nid = $node->nid;
  
          $node = node_load($nid);
          $node = node_view($node);
          $rendered_node = drupal_render($node);
          echo $rendered_node;
        }
        else {
          echo "<small>El recurso especificado ya no existe. Favor de eliminarlo de esta lista</small>";
          
        }
        
        echo "</div>";
        $i++;
      } ?>
      </div>
      <?php endif; ?>

      <?php if ($titulo_secc_coord) : ?>
      <div class="top-directorio-coordinaciones">
        <div class="titulo"><?php echo $titulo_secc_coord; ?></div>
      </div>
      <?php endif; ?>

      <?php if ($items_coord): ?>
      <div class="directorio-coordinaciones">
        <?php foreach ($items_coord as $item_coord): ?>
          <?php $fc_value = field_collection_field_get_entity($item_coord);
          $titulo_coord = $fc_value->field_t_tulo_c_dir_uac["und"][0]["value"];
          $nombre_coord = $fc_value->field_nombre_c_dir_uac["und"][0]["value"];
          $correo_coord = $fc_value->field_correo_c_dir_uac["und"][0]["value"];
          $telefono_coord = $fc_value->field_telefono_c_dir_uac["und"][0]["value"];
          $ubicacion_coord = $fc_value->field_ubicacion_c_dir_uac["und"][0]["value"]; ?>

          <div class="coordinacion">
            <div class="titulo-coordinacion"><?php echo $titulo_coord; ?></div>
			<?php if ($nombre_coord) : ?>
            <div class="nombre-coordinacion"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/nombre.png";?>"><?php echo $nombre_coord; ?></div>
			<?php endif; ?>
			<?php if ($correo_coord) : ?>
            <div class="correo-coordinacion"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/correo.png";?>"><?php echo $correo_coord; ?></div>
			<?php endif; ?>
			<?php if ($telefono_coord) : ?>
            <div class="telefono-coordinacion"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/telefono.png";?>"><?php echo $telefono_coord; ?></div>
			<?php endif; ?>
			<?php if ($ubicacion_coord) : ?>
            <div class="ubicacion-coordinacion"><img src="<?php echo $plantillas . "/images/assets/directorio_uac/ubicacion.png";?>"><?php echo $ubicacion_coord; ?></div>
			<?php endif; ?>
          </div>
          
        <?php endforeach; ?>
      </div>
      <?php endif; ?>

      <?php if ($titulo_secc_body): ?>
      <div class="top-directorio-cuerpo">
        <div class="titulo"><?php echo $titulo_secc_body; ?></div>
      </div>
      <?php endif; ?>
      
      <div class="directorio-cuerpo">
        <?php print render($content["body"]); ?>
      </div>
    </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</div>
