<style>
.node-type-p-gina-pila- .node--full { 
	overflow: hidden; float: left; 
}

.node-type-p-gina-pila- .pila-2-cols .pila-2-cols-col{
		width: 637px;
		height: 380px;
		float: left;
		margin-bottom: 25px;
    }

.node-type-p-gina-pila- .pila-3-cols .pila-3-cols-col{
	width: 416px;
	height: 344px;
	float: left;
	margin-bottom: 25px;
}

.node-type-p-gina-pila- .pila-4-cols .pila-4-cols-col{
	width: 306px;
	height: 246px;
	float: left;
	margin-bottom: 25px;
}

@media only screen and (min-width: 1300px) {

	/* 1 columna */
	/* 2 columnas */

		.node-type-p-gina-pila- .pila-2-cols .pila-2-cols-col:nth-child(odd){
			padding-right: 25px;
		}

		.node-type-p-gina-pila- .pila-2-cols .pila-2-cols-col:nth-child(even){
			padding: 0;
		}

		/* 3 columnas */

		.node-type-p-gina-pila- .pila-3-cols .pila-3-cols-col:nth-child(3n-2), .node-type-p-gina-pila- .pila-3-cols .pila-3-cols-col:nth-child(3n-1){
			padding-right: 25px;
		}

		.node-type-p-gina-pila- .pila-3-cols .pila-3-cols-col:nth-child(3n) {
			padding: 0;
		}

		/* 4 columnas */ 
		.node-type-p-gina-pila- .pila-4-cols .pila-4-cols-col:nth-child(4n-3), .node-type-p-gina-pila- .pila-4-cols .pila-4-cols-col:nth-child(4n-2), .node-type-p-gina-pila- .pila-4-cols .pila-4-cols-col:nth-child(4n-1){
			padding-right: 25px;
		}

		.node-type-p-gina-pila- .pila-4-cols .pila-4-cols-col:nth-child(4n) {
			padding: 0;
		}
   
    }

}


</style>