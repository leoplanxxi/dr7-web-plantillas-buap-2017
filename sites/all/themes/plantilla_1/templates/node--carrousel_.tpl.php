<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 global $base_url;
 $var['diseno'] = field_view_field("node", $node, 'field_dise_o')["#object"]->field_dise_o["und"][0]["value"];
 $var["color"] = field_view_field("node", $node, 'field_color_carrousel')["#object"]->field_color_carrousel["und"][0]["value"];
 $var["peso"] = field_view_field("node", $node, 'field_orden_carrousel')["#object"]->field_orden_carrousel["und"][0]["value"];
 $var["titulo"] = field_view_field("node", $node, 'field_mostrar_t_tulo_carrousel')["#object"]->field_mostrar_t_tulo_carrousel["und"][0]["value"];
?>
<script src="<?php echo $base_url; ?>/sites/all/libraries/slick/slick.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/sites/all/libraries/slick/slick.css" media="all">
<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>/sites/all/libraries/slick/slick-theme.css" media="all">

<script>
    var slider_certificaciones = function () {
        jQuery('.carrousel-1-col .carrousel').slick({
            slidesToShow: 6,
            slidesToScroll: 1, 
            variableWidth: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1500,
        });
        //autoplay: true,autoplaySpeed: 1000,
    }

    setTimeout(slider_certificaciones, 100);    
</script>
<style>
  #node-<?php print $node->nid; ?> .carrousel-parent {
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: <?php echo $var["color"]; ?>;
  }
</style>
<?php

$items = field_get_items('node', $node, 'field_elemento_carrousel');
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
	  
    ?>
	<div class="carrousel-1-col">
    				<?php if (user_is_logged_in()) : ?>
					<div class="peso"><?php echo $var["peso"]?></div>
				<?php endif; ?>
        <?php if ($var["titulo"] == 1): ?>
        <div class="titulo"><?php echo $title; ?></div>
        <?php endif; ?>
        <div class="carrousel-parent">
        <div class="carrousel">
		<?php
	

		foreach ($items as $item) {
            $fc_value = field_collection_field_get_entity($item);
            $link = $fc_value->field_link_gr_carr["und"][0]["url"];	
            $alt = $fc_value->field_link_gr_carr["und"][0]["title"];	
            $img_uri = $fc_value->field_imagen_gr_carr["und"][0]["uri"];

            $style = "carrousel";

			$derivative_uri = image_style_path($style, $img_uri);
			$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
			$new_image_url  = file_create_url($derivative_uri);

            if ($link == NULL) {
                $link = "#";
            }
			
			if ($fc_value->field_link_gr_carr["und"][0]["attributes"]["target"] == "_blank") {
				$attr_target = " target='_blank'";
			}
			else {
				$attr_target = " target='_self'";
			}

            echo "<div class='caballo'><a href='" . $link . "' " . $attr_target . "><img src='" . $new_image_url . "' alt='" . $alt . "' title='" . $alt . "'></a></div>";
        
		// Do something.
		}
			?>

		  </div></div></div>
  </div>

</div>
