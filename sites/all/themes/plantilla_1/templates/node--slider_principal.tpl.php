<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 global $base_url;
 $var['diseno'] = field_view_field("node", $node, 'field_dise_o')["#object"]->field_dise_o["und"][0]["value"];
 $var["animacion"] = field_view_field("node", $node, "field_animaci_n_slider")["#object"]->field_animaci_n_slider['und'][0]['value'];
?>
<?php if (!drupal_is_front_page()) : ?>
<script src="sites/all/libraries/flexslider/jquery.flexslider-min.js"></script>
<link type="text/css" rel="stylesheet" href="sites/all/libraries/flexslider/flexslider.css" media="all">
<script>
jQuery(window).load(function() {
  jQuery('#node-<?php echo $node->nid; ?> .flexslider').flexslider({
    animation: "slide"
	<?php
	if ($var["animacion"] == "Manual (No hay animación)") {
		echo ", slideshow: false";
	} else if ($var["animacion"] == "Automático (1 segundo)") {
		echo ", slideshowSpeed: 1000";
	} else if ($var["animacion"] == "Automático (3 segundos)") {
		echo ", slideshowSpeed: 3000";
	} else if ($var["animacion"] == "Automático (5 segundos)") {
		echo ", slideshowSpeed: 5000";
	}  else if ($var["animacion"] == "Automático (10 segundos)") {
		echo ", slideshowSpeed: 10000";
	}

	?>

  });
   jQuery('#node-<?php echo $node->nid; ?> .video-slider').trigger("play");;
});
</script>

<?php endif; ?>
<script>
jQuery(window).load(function() {
  jQuery('#node-<?php echo $node->nid; ?> .flexslider').flexslider({
    animation: "slide"
	<?php
	if ($var["animacion"] == "Manual (No hay animación)") {
		echo ", slideshow: false";
	} else if ($var["animacion"] == "Automático (1 segundo)") {
		echo ", slideshowSpeed: 1000";
	} else if ($var["animacion"] == "Automático (3 segundos)") {
		echo ", slideshowSpeed: 3000";
	} else if ($var["animacion"] == "Automático (5 segundos)") {
		echo ", slideshowSpeed: 5000";
	}  else if ($var["animacion"] == "Automático (10 segundos)") {
		echo ", slideshowSpeed: 10000";
	}

	?>

  });
  jQuery('#node-<?php echo $node->nid; ?> .video-slider').trigger("play");;
});
</script>
<style>
    .flexslider li video {
        width: 100%;
        max-height: 460px;
    }

    .flexslider .video{
        background: black;
    }
</style>
<?php if ($var["diseno"] == "Slider 1") :?>
<style>
.flexslider li .imagen-slider {
	width: 887px !important;
	height: 460px !important;
	float:right;
}

.flexslider .overlay {
	background: rgb(0,59,92);
    width: 325px;
	/*height: 100%;*/
    color: white;
    padding: 40px;
    position: absolute;
	float: left;
}

.flex-control-paging li a {
	background: rgba(0,201,240,0.5) !important;
}

.flex-control-nav {
    width: 405px !important;
    position: absolute !important;
    bottom: 0 !important;
    text-align: center !important;
}

.flex-control-paging li a.flex-active {
	background: rgb(0,201,240) !important;

}

.flexslider .titulo {
	font-size: 32px;
	color: rgb(0,201,240);
	font-family: sourcesans_bold;
	max-width: 325px;
}

.flexslider .descripcion {
	font-size: 22px;
	max-width: 325px;
	max-height: 325px;
	overflow: hidden;
	font-family: sourcesans_extralight;
}

.descripcion p {
	font-family: sourcesans_extralight;
}

</style>
<?php elseif ($var["diseno"] == "Slider 2"): ?>
<style>
.flexslider li .imagen-slider {
	width: 1300px !important;
	height: 460px !important;

}

.flexslider .overlay {
	position: absolute;
	bottom: 35px;
	background: rgba(7,7,7,0.5);
	color: white;
	left: 0;
    width: 500px;
    height: 160px !important;
    padding: 30px;
}

.flex-control-paging li a {
	background: rgba(0,201,240,0.5) !important;
}

.flex-control-nav {
    width: 1300px !important;
    position: absolute !important;
    bottom: 0 !important;
    text-align: center !important;
}

.flex-control-paging li a.flex-active {
	background: rgb(0,201,240) !important;

}

.flexslider .titulo {
	font-size:30px;
	font-family: sourcesans_bold;
	max-width: 100%;
	overflow: hidden;
}

.flexslider .descripcion {
	max-width: 100%;
	max-height: 120px;
    overflow: hidden;
}
</style>
<?php elseif ($var["diseno"] == "Slider 3"): ?>
<style>
.flexslider li .imagen-slider {
	width: 1300px !important;
	height: 460px !important;

}

.flexslider .overlay {
  position: absolute;
  bottom: 35px;
  background: rgba(7,7,7,0.5);
  color: white;
  left: 0;
  top: 0;
  width: 490px;
  height: 385px !important;
  padding: 40px;
}

.flex-control-paging li a {
	background: rgba(0,201,240,0.5) !important;
}

.flex-control-nav {
    width: 1300px !important;
    position: absolute !important;
    bottom: 0 !important;
    text-align: center !important;
}

.flex-control-paging li a.flex-active {
	background: rgb(0,201,240) !important;

}

.flexslider .titulo {
	font-size:30px;
	font-family: sourcesans_bold;
	max-width: 100%;
	overflow: hidden;
}

.flexslider .descripcion {
	max-width: 100%;
	max-height: 360px;
    overflow: hidden;
}
</style>
<?php endif; ?>
<?php

$items = field_get_items('node', $node, 'field_imagenes');
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);

    ?>
	<?php if ($var["diseno"] == "Slider 1"): ?>
	<div class="flexslider slider-1">
	<?php elseif ($var["diseno"] == "Slider 2"): ?>
	<div class="flexslider slider-2">
  <?php elseif ($var["diseno"] == "Slider 3"): ?>
    <div class="flexslider slider-3">
		<?php endif; ?>
		<ul class="slides">
		<?php


		foreach ($items as $item) {
		$fc_value = field_collection_field_get_entity($item);
        $select = $fc_value->field_desplegar_slider_principal["und"][0]["value"];
        $video = $fc_value->field_video_slider_principal["und"][0]["filename"];
		$imagen = $fc_value->field_imagen_slider_principal["und"][0]["filename"];
		$titulo = $fc_value->field_titulo["und"][0]["value"];
		$descripcion = $fc_value->field_descripci_n["und"][0]["value"];
		$link = $fc_value->field_link_slider["und"][0]["url"];
		$overlay = $fc_value->field_mostrar_overlay["und"][0]["value"];

		if ($fc_value->field_link_slider["und"][0]["attributes"]["target"] == "_blank") {
			$attr_target = " target='_blank'";
		}
		else {
			$attr_target = " target='_self'";
		}

		$img_uri = $fc_value->field_imagen_slider_principal["und"][0]["uri"];

    $img_alt = $fc_value->field_imagen_slider_principal["und"][0]["alt"];
    $img_title = $fc_value->field_imagen_slider_principal["und"][0]["title"];

			switch($var["diseno"]) {
				case "Slider 1":
					$style = "slider_principal__2_";
					break;
				case "Slider 2":
					$style = "slider_principal";
					break;
				default:
					break;
			}
			$derivative_uri = image_style_path($style, $img_uri);
			$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
			$new_image_url  = file_create_url($derivative_uri);
        if ($select == "Imagen") {
            if ($link == NULL) {
                echo "<li style='position: relative;'><img src='" . $new_image_url . "' alt='" . $img_alt . "' title='" . $img_title . "' class='imagen-slider'>";
            }
            else {
                echo "<li style='position: relative;'><a href='" . $link . "' " . $attr_target . "><img src='" . $new_image_url . "' alt='" . $img_alt . "' title='" . $img_title . "'></a>";
            }

            if ($overlay == 1) {
                echo "<div class='overlay'><div class='titulo'>" . $titulo. "</div>";
                echo "<div class='descripcion'>" . $descripcion . "</div></div>";
            }
			else if ($overlay == 0 && $var["diseno"] == "Slider 1")   {
				echo "<div class='overlay'><div class='titulo'>" . $titulo. "</div>";
                echo "<div class='descripcion'>" . $descripcion . "</div></div>";
			}

            echo "</li>";
        }
        else if ($select == "Video") {
             if ($link == NULL) {
                echo "<li style='position: relative;' class='video'><video src='sites/default/files/" . $video . "' loop muted class='video-slider'></video>";
            }
            else {
                echo "<li style='position: relative;' class='video'><a href='" . $link . "' " . $attr_target . "><video src='sites/default/files/" . $video . "' autoplay loop muted></video></a>";
            }

            if ($overlay == 1) {
                echo "<div class='overlay'><div class='titulo'>" . $titulo. "</div>";
                echo "<div class='descripcion'>" . $descripcion . "</div></div>";
            }
			else if ($overlay == 0 && $var["diseno"] == "Slider 1")   {
				echo "<div class='overlay'><div class='titulo'>" . $titulo. "</div>";
                echo "<div class='descripcion'>" . $descripcion . "</div></div>";
			}
            echo "</li>";
        }

		// Do something.
		}
			?>
			</ul>
		  </div>
  </div>


</div>
