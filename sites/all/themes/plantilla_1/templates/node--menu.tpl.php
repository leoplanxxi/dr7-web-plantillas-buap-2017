<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 global $base_url;
?>

<?php

$var['diseno'] = field_view_field("node", $node, 'field_dise_o_menu')["#object"]->field_dise_o_menu["und"][0]["value"];
$var['links'] = field_view_field("node", $node, 'field_link')["#object"]->field_link["und"];
$var['cols'] = field_view_field("node", $node, 'field_columnas_menu')["#object"]->field_columnas_menu["und"][0]["value"];
$var['img'] = field_view_field("node", $node, 'field_imagen_fondo')["#object"]->field_imagen_fondo["und"][0];
$var['img_link'] = field_view_field("node", $node, "field_link_imagen_menu")["#object"]->field_link_imagen_menu["und"][0];
$var['peso'] = field_view_field("node", $node, 'field_orden_menu')["#object"]->field_orden_menu["und"][0]["value"];

if ($var['diseno'] == "Azul") {
	$attr["diseno"] = "diseno-2";
	if ($var["cols"] == "2") {
		$attr["cols"] = "menu-2-cols";
	}
	else if ($var["cols"] == "Columna Izquierda" || $var["cols"] == "Columna Derecha" || $var["cols"] == "Lateral Interior") {
		if ($var["img"] != NULL) {
			$attr["cols"] = "menu-col menu-col-img";
		}
		else {
			$attr["cols"] = "menu-col";
		}
		
	}
}
else if ($var['diseno'] == "Blanco") {
	$attr["diseno"] = "diseno-2-neg";
	if ($var["cols"] == "2") {
		$attr["cols"] = "menu-2-cols";
	}
	else if ($var["cols"] == "Columna Izquierda" || $var["cols"] == "Columna Derecha" || $var["cols"] == "Lateral Interior") {
		$attr["cols"] = "menu-col";
	}
}
else if ($var["diseno"] == "Hover") {
	$attr["diseno"] = "diseno-1";
	if ($var["cols"] == "3") {
		$attr["cols"] = "menu-3-cols";
	} else if ($var["cols"] == "4") {
		$attr["cols"] = "menu-4-cols";
	} else if ($var["cols"] == "2") {
		$attr["cols"] = "menu-2-cols";
	}
}

else if ($var["diseno"] == "Overlay") {
	$attr["diseno"] = "diseno-4";
	if ($var["cols"] == "3") {
		$attr["cols"] = "menu-3-cols";
	} else if ($var["cols"] == "4") {
		$attr["cols"] = "menu-4-cols";
	} else if ($var["cols"] == "2") {
		$attr["cols"] = "menu-2-cols";
	}
}

else if ($var["diseno"] == "Caret") {
	$attr["diseno"] = "diseno-3";
	if ($var["cols"] == "4") {
		$attr["cols"] = "menu-4-cols";
	} else if ($var["cols"] == "3") {
		$attr["cols"] = "menu-3-cols";
	} else if ($var["cols"] == "2") {
		$attr["cols"] = "menu-2-cols";
	}
}

else if ($var["diseno"] == "Menu Central") {
	$attr["diseno"] = "diseno-cc";
	if ($var["cols"] == "Columna Central") {
		$attr["cols"] = "menu-cc";
	}
}
/*echo "<pre>";
var_dump($var);
echo "</pre>"; */
?>

<?php if ($var["cols"] == "Lateral Interior"): ?>
<style>
.menu-col .logo_menu {
    vertical-align: middle;
    height: 55px;
}
</style>
<script>
    var contador = 1;
    jQuery(document).ready(function() {
        
		var ventana_ancho = jQuery(window).width();    
        if (ventana_ancho < 1025)
            {
                jQuery("#logoImagen").attr("src","sites/all/themes/plantilla_1/images/menu_responsive_azul.png");
                jQuery('.menu-col.diseno-2 .titulo').click(function(){
                    // $('nav').toggle(); 
                    if(contador == 1){
                        jQuery('.menu-col.diseno-2 .menu').slideDown();
						contador = 0;
						console.log("entre aca"); 
                    } 
                    else {
                        contador = 1;
                        jQuery('.menu-col.diseno-2 .menu').slideUp();
                    }
                });

                jQuery("#logoImagen").attr("src","sites/all/themes/plantilla_1/images/menu_responsive_blanco.png");
                jQuery('.menu-col.diseno-2-neg .titulo').click(function(){
                    // $('nav').toggle(); 
                    if(contador == 1){
                        jQuery('.menu-col.diseno-2-neg .menu').slideDown();
                        contador = 0;
                    } 
                    else {
                        contador = 1;
                        jQuery('.menu-col.diseno-2-neg .menu').slideUp();
                    }
                }); 
            }     
 });
</script>
<?php endif; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix recursos-<?php echo $var["cols"]; ?>-cols" <?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
	  
    ?>

	<?php 
		if ($var["diseno"] == "Azul" || $var["diseno"] == "Blanco") {
			if ($var["cols"] == "2" || $var["cols"] == "Columna Izquierda" || $var["cols"] == "Columna Derecha" || $var["cols"] == "Lateral Interior"): ?>
				
				<?php
					//Generación dinámica de JavaScript / jQuery para acomodar correctamente los elementos en los menus del superpanel

					if ($var["img"] == NULL) {
						if ($var["cols"] == "Columna Izquierda") {
							$col_izq_jq = 'titulo_height_ci = jQuery(".columna-izquierda #node-' . $node->nid . ' .titulo").outerHeight();
							diff = titulo_height_ci-76;
							menu_ul_height_ci = 692 - diff;
							
							jQuery(".columna-izquierda #node-' . $node->nid . ' .menu ul").height(menu_ul_height_ci);';
						}
						else {
							$col_der_jq = 'titulo_height_cd = jQuery(".columna-derecha #node-' . $node->nid . ' .titulo").outerHeight();
							diff = titulo_height_cd-76;
							menu_ul_height_cd = 692 - diff;

							jQuery(".columna-derecha #node-' . $node->nid . ' .menu ul").height(menu_ul_height_cd);';
						}
						
					}
					else {
						if ($var["cols"] == "Columna Izquierda") {
							$col_izq_jq = 'titulo_height_ci = jQuery(".columna-izquierda #node-' . $node->nid . ' .titulo").outerHeight();
							imagen_height = jQuery(".columna-izquierda #node-' . $node->nid . ' .imagen_top").outerHeight();
							diff = imagen_height+titulo_height_ci;
							menu_ul_height_ci = 780 - diff;
							
							jQuery(".columna-izquierda #node-' . $node->nid . ' .menu ul").height(menu_ul_height_ci);';
						}
						else {
							$col_der_jq = 'titulo_height_cd = jQuery(".columna-derecha #node-' . $node->nid . ' .titulo").outerHeight();
							imagen_height = jQuery(".columna-derecha #node-' . $node->nid . ' .imagen_top").outerHeight();
							diff = imagen_height+titulo_height_cd;
							menu_ul_height_cd = 780 - diff;

							jQuery(".columna-derecha #node-' . $node->nid . ' .menu ul").height(menu_ul_height_cd);';
						}
					}
					
				?>


				<script>
					jQuery(window).load(function() {

						/* Columna izquierda */
						<?php echo $col_izq_jq; ?>

						/*Columna derecha*/		
						<?php echo $col_der_jq; ?>
						
					});
				</script>
				

				<div class="<?php echo $attr["cols"] . " " . $attr["diseno"]; ?>">
					<?php 
					
					if ($var["cols"] == "Columna Derecha" || $var["cols"] == "Columna Izquierda" || $var["cols"] == "Lateral Interior") {
						if ($var["img"] != NULL) {
								$img_uri = $var["img"]["uri"];
								
								if (field_view_field("node", $node, "field_link_imagen_menu")["#object"]->field_link_imagen_menu["und"][0]["attributes"]["target"] == "_blank") {
									$attr_target_i = " target='_blank'";
								}
								else {
									$attr_target_i = " target='_self'";
								}
							

								$style = "menu_lateral";
								$derivative_uri = image_style_path($style, $img_uri);
								$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
								$new_image_url  = file_create_url($derivative_uri);
								echo "<div class='imagen_top'>";
								if ($var["img_link"] == NULL) {
									echo "<img src='" . $new_image_url . "' /></div>";
								}
								else {
									echo "<a href='" . $var["img_link"]["url"] . "' " . $attr_target_i . ">";
									echo "<img src='" . $new_image_url . "' /></div>";
									echo "</a>";
								}

						}
					} ?>
									<?php if (user_is_logged_in()) : ?>
					<div class="peso"><?php echo $var["peso"]?></div>
				<?php endif; ?>
					<div class="titulo">
						<p><?php echo $title; ?><img id="logoImagen" class="logo_menu" src=""></p>
					</div>
					<div class="menu">
						<ul>
							<?php foreach ($var["links"] as $link) {
								
								if ($link["attributes"]["target"] == "_blank") {
									$attr_target = " target='_blank'";
								}
								else {
									$attr_target = " target='_self'";
								}
								echo "<li><a href='" . $link["url"] . "' " . $attr_target . ">" . $link["title"] . "</a></li>";
							} ?>
						</ul>
					</div>
				</div>
			<?php
			
			else: ?>
				<p>Error de configuración.</p>
			<?php endif; 
		} else if ($var["diseno"] == "Hover") {
			$img_uri = $var["img"]["uri"];

			switch($var["cols"]) {
				case "2":
					$style = "2_columnas";
					break;
				case "3":
					$style = "3_columnas";
					break;
				case "4":
					$style = "4_columnas";
					break;
				default: 
					break;
			}
			$derivative_uri = image_style_path($style, $img_uri);
			$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
			$new_image_url  = file_create_url($derivative_uri);

			if ($var["cols"] == "2" || $var["cols"] == "3" || $var["cols"] == "4"): ?>
				
				<div class="<?php echo $attr["cols"] . " " . $attr["diseno"]; ?>">
				<?php if (user_is_logged_in()) : ?>
					<div class="peso"><?php echo $var["peso"]?></div>
				<?php endif; ?>
					<img src="<?php echo $new_image_url; ?>" />
					<div class="titulo">
						<p><?php echo $title; ?></p>
					</div>
					<div class="menu">
						<ul>
							<?php foreach ($var["links"] as $link) {
								if ($link["attributes"]["target"] == "_blank") {
									$attr_target = " target='_blank'";
								}
								else {
									$attr_target = " target='_self'";
								}
								echo "<li><a href='" . $link["url"] . "' " . $attr_target . ">" . $link["title"] . "</a></li>";
								//echo "<li><a href='" . $link["url"] . "'>" . $link["title"] . "</a></li>";
							} ?>
						</ul>
					</div>
				</div>
				

				<?php else: ?>
				<p>Error de configuración.</p>
			<?php endif; 
		}
		else if ($var["diseno"] == "Caret") { 
			$img_uri = $var["img"]["uri"];

			switch($var["cols"]) {
				case "2":
					$style = "2_columnas";
					break;
				case "4":
					$style = "4_columnas";
					break;
				case "3":
					$style = "3_columnas";
					break;
				default: 
					break;
			}
			$derivative_uri = image_style_path($style, $img_uri);
			$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
			$new_image_url  = file_create_url($derivative_uri); 
			
			if ($var["cols"] == "2" || $var["cols"] == "4" || $var["cols"] == "3"): ?>
				 <div class="<?php echo $attr["cols"] . " " . $attr["diseno"]; ?>">
				 	<?php if (user_is_logged_in()) : ?>
					<div class="peso" style="z-index:900"><?php echo $var["peso"]?></div>
				<?php endif; ?>
				<img src="<?php echo $new_image_url; ?>">
				<div class="animacion">
					<div class="caret">
					</div>
					<div class="menu">
						<ul>
							<?php foreach ($var["links"] as $link) {
								if ($link["attributes"]["target"] == "_blank") {
									$attr_target = " target='_blank'";
								}
								else {
									$attr_target = " target='_self'";
								}
								echo "<li><a href='" . $link["url"] . "' " . $attr_target . ">" . $link["title"] . "</a></li>";
								//echo "<li><a href='" . $link["url"] . "'>" . $link["title"] . "</a></li>";
							} ?>
						</ul>
					</div>
				</div>
				<div class="titulo">
					<?php echo $title; ?>
				</div>
			</div> 
			
		<?php endif; 
		}
		else if ($var["diseno"] == "Menu Central") { 
			$img_uri = $var["img"]["uri"];

			switch($var["cols"]) {
				case "Columna Central":
					$style = "menu_central";
					break;
				default: 
					break;
			}
			$derivative_uri = image_style_path($style, $img_uri);
			$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
			$new_image_url  = file_create_url($derivative_uri);
			
			if ($var["cols"] == "Columna Central"): ?>
				<div class="menu-cc">
					<?php if (user_is_logged_in()) : ?>
					<div class="peso"><?php echo $var["peso"]?></div>
				<?php endif; ?>
					<img src="<?php echo $new_image_url; ?>">
					<div class="titulo"><?php echo $title; ?></div>
					<ul class="menu">
						<?php foreach ($var["links"] as $link) {
							if ($link["attributes"]["target"] == "_blank") {
									$attr_target = " target='_blank'";
								}
								else {
									$attr_target = " target='_self'";
								}
								echo "<li><a href='" . $link["url"] . "' " . $attr_target . ">" . $link["title"] . "</a></li>";
							//echo "<li><a href='" . $link["url"] . "'>" . $link["title"] . "</a></li>";
						} ?>
					</ul>
				</div>
			
		<?php endif; 
		}
		
		else if ($var["diseno"] == "Overlay") {
			$img_uri = $var["img"]["uri"];

			switch($var["cols"]) {
				case "2":
					$style = "2_columnas";
					break;
				case "4":
					$style = "4_columnas";
					break;
				case "3":
					$style = "3_columnas";
					break;
				default: 
					break;
			}
			$derivative_uri = image_style_path($style, $img_uri);
			$success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
			$new_image_url  = file_create_url($derivative_uri); 

			if ($var["cols"] == "2" || $var["cols"] == "4" || $var["cols"] == "3"): ?>
				 <div class="<?php echo $attr["cols"] . " " . $attr["diseno"]; ?>">
				 	<?php if (user_is_logged_in()) : ?>
					<div class="peso" style="z-index:900"><?php echo $var["peso"]?></div>
				<?php endif; ?>
				<img src="<?php echo $new_image_url; ?>">
				<div class="overlay-menu"></div>
				<div class="titulo">
					<?php echo $title; ?>
				</div>
					<div class="menu">
						<ul>
							<?php foreach ($var["links"] as $link) {
								if ($link["attributes"]["target"] == "_blank") {
									$attr_target = " target='_blank'";
								}
								else {
									$attr_target = " target='_self'";
								}
								echo "<li><a href='" . $link["url"] . "' " . $attr_target . ">" . $link["title"] . "</a></li>";
								//echo "<li><a href='" . $link["url"] . "'>" . $link["title"] . "</a></li>";
							} ?>
						</ul>
					</div>

			</div> 
			
		<?php endif; 
		}?>
		
  </div>



</div>
