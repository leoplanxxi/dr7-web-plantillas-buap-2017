
<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 global $base_url;
?>
<style>
.pane-page-content .node__title {
    display: none;
}

.pila-dinamico { float: left; padding-top: 16px; padding-bottom: 16px;}
</style>
<?php

//$items = field_get_items('node', $node, 'field_imagenes');
$var['links'] = field_view_field("node", $node, 'field_recurso_pila')["#object"]->field_recurso_pila["und"];
$var['css'] = field_view_field("node", $node, 'field_aplicar_css_pila')["#object"]->field_aplicar_css_pila["und"][0]["value"];
$var["cols_css"] = field_view_field("node", $node, 'field_columnas_css_pila')["#object"]->field_columnas_css_pila["und"][0]["value"];
$var['tipo'] = field_view_field("node", $node, 'field_tipo_pila')["#object"]->field_tipo_pila["und"][0]["value"];

?>

<?php 

  if ($var["css"] == "1") {
    switch ($var["cols_css"]) {
      case "2":
        $cols_css = "pila-2-cols";
        break;
      case "3":
        $cols_css = "pila-3-cols";
        break;
      case "4":
        $cols_css = "pila-4-cols";
        break;
      default:
        $cols_css = "pila-nocols";
        break;
    }
  }
  else {
    $cols_css = "pila-nocols";
  }
 
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
	  
    ?>
 
    <?php if ($var["css"] == "1"): ?>
      <?php require "node--p_gina_pila_.css.php"; ?>
    <?php endif; ?>
    <div class="content-pila <?php echo $cols_css; ?>">

    <?php if ($var["tipo"] == "Estática"): ?>
    <?php 
    $i = 1;
    foreach ($var["links"] as $link) {
        echo "<div class='pila-" . $i . " pila-estatico " .  $cols_css . "-col'>";
        $alias = str_replace($base_url,"", $link);
        $alias = str_replace("/?q=", "", $alias);
        $alias = $alias["url"];
        $path = drupal_lookup_path("source", $alias);

        if ($path != false) {
          $node = menu_get_object("node", 1, $path);
          $nid = $node->nid;
  
          $node = node_load($nid);
          $node = node_view($node);
          $rendered_node = drupal_render($node);
          echo $rendered_node;
        }
        else {
          echo "<small>El recurso especificado ya no existe. Favor de eliminarlo de esta lista</small>";
          
        }
        
        echo "</div>";
        $i++;
    } ?>
    <?php elseif ($var["tipo"] == "Dinámica"): ?>
      <?php 
      $i = 1;
      foreach ($var["links"] as $link) {
          echo "<div class='pila-" . $i . " pila-dinamico " .  $cols_css . "-col'>";
          $alias = str_replace($base_url,"", $link);
          $alias = str_replace("/?q=", "", $alias);
          $alias = $alias["url"];
          $path = drupal_lookup_path("source", $alias);
          if ($path != false) {
            $node = menu_get_object("node", 1, $path);
            $nid = $node->nid;
  
            $node = node_load($nid);
            $node = node_view($node);
            $rendered_node = drupal_render($node);
            echo $rendered_node;
          }
          else {
            echo "<small>El recurso especificado ya no existe. Favor de eliminarlo de esta lista</small>";
          }
         
          echo "</div>";
          $i++;
      } ?>
    <?php endif; ?>
    </div>
    </div>
