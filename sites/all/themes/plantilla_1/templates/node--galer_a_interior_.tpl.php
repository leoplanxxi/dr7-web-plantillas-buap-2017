<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
 global $base_url;
?>
<?php

//$items = field_get_items('node', $node, 'field_imagenes');
$var['diseno'] = field_view_field("node", $node, 'field_presentaci_n_galeria_i')["#object"]->field_presentaci_n_galeria_i["und"][0]["value"];
$var['imgs'] = field_view_field("node", $node, 'field_imagenes_galeria_i')["#object"]->field_imagenes_galeria_i["und"];

?>
<style>


</style>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);

    ?>
    <?php if ($var["diseno"] == "Mosaicos") : ?>

    <div class="col-principal">
    <?php print render($content["field_imagenes_galeria_i"]); ?>
  </div>
  <?php elseif ($var["diseno"] == "Mosaicos (4 cols)"): ?>

  <script>
    jQuery(window).bind('load', function() {
      jQuery(".grupo-colorbox").colorbox({rel:'grupo-colorbox', maxWidth:'95%', maxHeight:'95%'});
    });
  		
  </script>
    <div class="field field--name-field-imagenes-galeria-i field--type-image field--label-hidden">
      <div class="field__items">
        <?php
              $i = 0;
              foreach ($var["imgs"] as $item) {

                  $img_uri = $item["uri"];

                  $img_fs = str_replace("public://", $base_url . "/sites/default/files/", $item["uri"]);
                  $style = "4_columnas";

                  $derivative_uri = image_style_path($style, $img_uri);
                  $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
                  $new_image_url  = file_create_url($derivative_uri);

                  /*if ($i != 0) echo "<div class='item'>";
                  else echo */

                  if ($i%2 == 0) $even = "even";
                  else $even = "odd"; 

                  echo "<div class='field__item " . $even . "'>";
                  //echo '<a href="' . $img_fs . '" title="' . $node->title . '" class="colorbox init-colorbox-processed cboxElement colorbox-load" data-colorbox-gallery="gallery-node-1288-field_imagenes_galeria_i-c3C-l9vzqTU" data-cbox-img-attrs="{&quot;title&quot;: &quot;&quot;, &quot;alt&quot;: &quot;&quot;}"><img typeof="foaf:Image" src="' . $new_image_url . '" width="306" height="246" alt="" title=""></a>';
                    echo '<a href="' . $img_fs . '" title="' . $node->title . '" class="grupo-colorbox cboxElement"><img src="' . $new_image_url . '" width="306" height="246" alt="" title=""></a>';
                  echo "</div>";
                  $i++;
              }
          ?>
      </div>
    </div>
  
  
  <?php elseif ($var["diseno"] == "Diapositivas"): ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css" integrity="sha256-BeFg+v/PNshQc74GF4K1AJR50JoamZ6HhukelxXHAVI=" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider.min.js" integrity="sha256-NZC1SCKsqaTNlJRuLR1O6cOZGUthQpzWe/UjN9Tfrf4=" crossorigin="anonymous"></script>
  <style>
   ol.flex-control-nav {
	display: none;
}
  </style>

  <script>

  jQuery(document).ready(function(){
    jQuery('#carousel').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 210,
      itemMargin: 5,
      asNavFor: "#flexslider"
    });
    jQuery('#flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: "#carousel"
    });
  });


  </script>
  <div class="flexslider" id="flexslider">
		<ul class="slides">
        <?php
            $i = 0;
            foreach ($var["imgs"] as $item) {

                $img_uri = $item["uri"];
                $style = "slider_principal";

                $derivative_uri = image_style_path($style, $img_uri);
                $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
                $new_image_url  = file_create_url($derivative_uri);

                /*if ($i != 0) echo "<div class='item'>";
                else echo */
			          echo "<li style='position: relative'>";

                        echo "<img src='" . $new_image_url . " '>";

                echo "</li>";
                $i++;
            }
        ?>
		</ul>
    </div>

    <div class="flexslider" id="carousel">
		<ul class="slides">
        <?php
            $i = 0;
            foreach ($var["imgs"] as $item) {
              foreach ($var["imgs"] as $item) {

                  $img_uri = $item["uri"];
                  $style = "slider_principal";

                  $derivative_uri = image_style_path($style, $img_uri);
                  $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $img_uri, $derivative_uri);
                  $new_image_url  = file_create_url($derivative_uri);

                  /*if ($i != 0) echo "<div class='item'>";
                  else echo */
                  echo "<li style='position: relative'>";

                          echo "<img src='" . $new_image_url . " '>";

                  echo "</li>";
                $i++;
            }
          }
        ?>
		</ul>
    </div>

<?php endif; ?>



</div>
