<?php
require("inc/inc_cssjs.php");

error_reporting(E_ALL);
ini_set('display_errors', 1);
$json_in = file_get_contents("data/fields_example.json");
$json_in = json_decode($json_in);

?>
<form action="dev_form.php" method="POST"  enctype="multipart/form-data"> 
<?php
foreach ($json_in->fields as $field) {
	$label = $field->label;
	$id = $field->id;
	$type = $field->type;
	$name = $field->name;
	$value = $field->value;
	$class = $field->class;
	
	echo "<label for='$id'>$label</label>";
	echo "<br>";
	if ($type != "textarea") {
		echo "<input type='$type' name='$name' id='$id' value='$value' class='$class' />";
	}
	else {
		echo "<textarea name='$name' id='$id' class='$class'>$value</textarea>";
	}
	echo "</p>";
}
?>
<input type="submit" value="Guardar" class="btn btn-success">
</form>