<?php

/**
 * @file
 * configuracion.features.inc
 */

/**
 * Implements hook_default_config_pages_type().
 */
function configuracion_default_config_pages_type() {
  $items = array();
  $items['configuraci_n_de_sitio'] = entity_import('config_pages_type', '{
    "type" : "configuraci_n_de_sitio",
    "label" : "Configuraci\\u00f3n de sitio",
    "weight" : "0",
    "data" : {
      "menu" : { "path" : "admin\\/config\\/config_sitio", "type" : "6" },
      "context" : { "group" : {
          "config_pages:language" : 0,
          "config_pages:host" : 0,
          "config_pages:domain" : 0
        }
      }
    },
    "rdf_mapping" : []
  }');
  $items['scripts_y_css_personalizados'] = entity_import('config_pages_type', '{
    "type" : "scripts_y_css_personalizados",
    "label" : "Scripts y CSS personalizados",
    "weight" : "0",
    "data" : {
      "menu" : { "path" : "admin\\/config\\/js_css", "type" : "6" },
      "context" : { "group" : {
          "config_pages:language" : 0,
          "config_pages:host" : 0,
          "config_pages:domain" : 0
        }
      }
    },
    "rdf_mapping" : []
  }');
  return $items;
}
