<?php

/**
 * @file
 * configuracion.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function configuracion_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_desplegar_1_columna'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_desplegar_1_columna'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_desplegar_1_columna',
    'label' => 'Desplegar 1 columna',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_desplegar_2_columnas'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_desplegar_2_columnas'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_desplegar_2_columnas',
    'label' => 'Desplegar 2 columnas',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_desplegar_3_columnas'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_desplegar_3_columnas'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_desplegar_3_columnas',
    'label' => 'Desplegar 3 columnas',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 12,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_desplegar_4_columnas'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_desplegar_4_columnas'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 11,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_desplegar_4_columnas',
    'label' => 'Desplegar 4 columnas',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 15,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_desplegar_panel_noticias_d'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_desplegar_panel_noticias_d'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 13,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_desplegar_panel_noticias_d',
    'label' => 'Desplegar panel noticias del sitio',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 19,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_direcci_n_sitio'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_direcci_n_sitio'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_direcci_n_sitio',
    'label' => 'Dirección',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 0,
          'ds_code' => 0,
          'filtered_html' => 0,
          'full_html' => 'full_html',
          'php_code' => 0,
          'plain_text' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => -50,
            ),
            'ds_code' => array(
              'weight' => 12,
            ),
            'filtered_html' => array(
              'weight' => 0,
            ),
            'full_html' => array(
              'weight' => 1,
            ),
            'php_code' => array(
              'weight' => 11,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
          ),
        ),
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_id_analytics'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_id_analytics'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 20,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_id_analytics',
    'label' => 'ID Analytics',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 'ace_editor',
          'ds_code' => 'ds_code',
          'filtered_html' => 'filtered_html',
          'full_html' => 'full_html',
          'menu_lateral_interior' => 'menu_lateral_interior',
          'php_code' => 'php_code',
          'plain_text' => 'plain_text',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => 4,
            ),
            'ds_code' => array(
              'weight' => 3,
            ),
            'filtered_html' => array(
              'weight' => 1,
            ),
            'full_html' => array(
              'weight' => 0,
            ),
            'menu_lateral_interior' => array(
              'weight' => 0,
            ),
            'php_code' => array(
              'weight' => 2,
            ),
            'plain_text' => array(
              'weight' => 5,
            ),
          ),
        ),
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 21,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_imagen_derecha_superpanel'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_imagen_derecha_superpanel'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_imagen_derecha_superpanel',
    'label' => 'Imagen Derecha Superpanel',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_img_izquierda_superpanel'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_img_izquierda_superpanel'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_img_izquierda_superpanel',
    'label' => 'Imagen Izquierda Superpanel',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_link_derecha_superpanel'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_link_derecha_superpanel'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_link_derecha_superpanel',
    'label' => 'Link Derecha Superpanel',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'user',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_link_izquierdo_superpanel'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_link_izquierdo_superpanel'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_link_izquierdo_superpanel',
    'label' => 'Link Izquierdo Superpanel',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'user',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_mostrar_pesos_en_recursos'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_mostrar_pesos_en_recursos'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 16,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_mostrar_pesos_en_recursos',
    'label' => 'Mostrar pesos en recursos',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 20,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_nombre_del_sitio'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_nombre_del_sitio'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_nombre_del_sitio',
    'label' => 'Nombre del sitio',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 0,
          'ds_code' => 0,
          'filtered_html' => 0,
          'full_html' => 'full_html',
          'php_code' => 0,
          'plain_text' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => -50,
            ),
            'ds_code' => array(
              'weight' => 12,
            ),
            'filtered_html' => array(
              'weight' => 0,
            ),
            'full_html' => array(
              'weight' => 1,
            ),
            'php_code' => array(
              'weight' => 11,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
          ),
        ),
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_1_columna'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_1_columna'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 15,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_1_columna',
    'label' => 'Peso 1 columna',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_2_cols_var'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_2_cols_var'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 17,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_2_cols_var',
    'label' => 'Peso 2 cols var',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 11,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_2_columnas'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_2_columnas'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_2_columnas',
    'label' => 'Peso 2 columnas',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 10,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_3_cols_var'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_3_cols_var'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 18,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_3_cols_var',
    'label' => 'Peso 3 cols var',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 14,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_3_columnas'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_3_columnas'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_3_columnas',
    'label' => 'Peso 3 columnas',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 13,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_4_cols_var'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_4_cols_var'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 19,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_4_cols_var',
    'label' => 'Peso 4 cols var',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 17,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_peso_4_columnas'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_peso_4_columnas'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 12,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_peso_4_columnas',
    'label' => 'Peso 4 columnas',
    'required' => 0,
    'settings' => array(
      'range' => 20,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'weight',
      'settings' => array(
        'range' => 10,
      ),
      'type' => 'weight_selector',
      'weight' => 16,
    ),
  );

  // Exported field_instance:
  // 'config_pages-configuraci_n_de_sitio-field_superpanel_sitio'.
  $field_instances['config_pages-configuraci_n_de_sitio-field_superpanel_sitio'] = array(
    'bundle' => 'configuraci_n_de_sitio',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_superpanel_sitio',
    'label' => 'El sitio contiene un superpanel',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'config_pages-scripts_y_css_personalizados-field_css'.
  $field_instances['config_pages-scripts_y_css_personalizados-field_css'] = array(
    'bundle' => 'scripts_y_css_personalizados',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'ace_editor',
        'settings' => array(
          'autocomplete' => FALSE,
          'autowrap' => TRUE,
          'font-size' => '12pt',
          'height' => '200px',
          'invisibles' => FALSE,
          'line-numbers' => FALSE,
          'linehighlighting' => TRUE,
          'print-margin' => FALSE,
          'syntax' => 'html',
          'tabsize' => 2,
          'theme' => 'cobalt',
          'width' => '100%',
        ),
        'type' => 'ace_editor_code_readonly_formatter',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_css',
    'label' => 'CSS',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 0,
          'ds_code' => 0,
          'filtered_html' => 0,
          'full_html' => 0,
          'menu_lateral_interior' => 0,
          'php_code' => 'php_code',
          'plain_text' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => 4,
            ),
            'ds_code' => array(
              'weight' => 3,
            ),
            'filtered_html' => array(
              'weight' => 1,
            ),
            'full_html' => array(
              'weight' => 0,
            ),
            'menu_lateral_interior' => array(
              'weight' => 0,
            ),
            'php_code' => array(
              'weight' => 2,
            ),
            'plain_text' => array(
              'weight' => 5,
            ),
          ),
        ),
      ),
      'display_summary' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'config_pages-scripts_y_css_personalizados-field_javascript'.
  $field_instances['config_pages-scripts_y_css_personalizados-field_javascript'] = array(
    'bundle' => 'scripts_y_css_personalizados',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'ace_editor',
        'settings' => array(
          'autocomplete' => FALSE,
          'autowrap' => TRUE,
          'font-size' => '12pt',
          'height' => '200px',
          'invisibles' => FALSE,
          'line-numbers' => 0,
          'linehighlighting' => 1,
          'print-margin' => FALSE,
          'syntax' => 'html',
          'tabsize' => 2,
          'theme' => 'cobalt',
          'width' => '100%',
        ),
        'type' => 'ace_editor_code_readonly_formatter',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_javascript',
    'label' => 'JavaScript',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 0,
          'ds_code' => 0,
          'filtered_html' => 0,
          'full_html' => 0,
          'menu_lateral_interior' => 0,
          'php_code' => 'php_code',
          'plain_text' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => 4,
            ),
            'ds_code' => array(
              'weight' => 3,
            ),
            'filtered_html' => array(
              'weight' => 1,
            ),
            'full_html' => array(
              'weight' => 0,
            ),
            'menu_lateral_interior' => array(
              'weight' => 0,
            ),
            'php_code' => array(
              'weight' => 2,
            ),
            'plain_text' => array(
              'weight' => 5,
            ),
          ),
        ),
      ),
      'display_summary' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('CSS');
  t('Desplegar 1 columna');
  t('Desplegar 2 columnas');
  t('Desplegar 3 columnas');
  t('Desplegar 4 columnas');
  t('Desplegar panel noticias del sitio');
  t('Dirección');
  t('El sitio contiene un superpanel');
  t('ID Analytics');
  t('Imagen Derecha Superpanel');
  t('Imagen Izquierda Superpanel');
  t('JavaScript');
  t('Link Derecha Superpanel');
  t('Link Izquierdo Superpanel');
  t('Mostrar pesos en recursos');
  t('Nombre del sitio');
  t('Peso 1 columna');
  t('Peso 2 cols var');
  t('Peso 2 columnas');
  t('Peso 3 cols var');
  t('Peso 3 columnas');
  t('Peso 4 cols var');
  t('Peso 4 columnas');

  return $field_instances;
}
