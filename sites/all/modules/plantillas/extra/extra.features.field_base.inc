<?php
/**
 * @file
 * extra.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function extra_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_punto_de_acceso_pf'.
  $field_bases['field_punto_de_acceso_pf'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_punto_de_acceso_pf',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_titulo_pf'.
  $field_bases['field_titulo_pf'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_titulo_pf',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
