<?php
/**
 * @file
 * extra.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function extra_taxonomy_default_vocabularies() {
  return array(
    'p_ginas_frontales' => array(
      'name' => 'Páginas Frontales',
      'machine_name' => 'p_ginas_frontales',
      'description' => 'Desde esta interfaz se definen páginas frontales secundarias',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
