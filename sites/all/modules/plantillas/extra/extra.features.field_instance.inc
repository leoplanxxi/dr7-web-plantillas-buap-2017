<?php
/**
 * @file
 * extra.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function extra_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-p_ginas_frontales-field_punto_de_acceso_pf'.
  $field_instances['taxonomy_term-p_ginas_frontales-field_punto_de_acceso_pf'] = array(
    'bundle' => 'p_ginas_frontales',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_punto_de_acceso_pf',
    'label' => 'Punto de acceso',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 'ace_editor',
          'ds_code' => 'ds_code',
          'filtered_html' => 'filtered_html',
          'full_html' => 'full_html',
          'menu_lateral_interior' => 'menu_lateral_interior',
          'php_code' => 'php_code',
          'plain_text' => 'plain_text',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => 4,
            ),
            'ds_code' => array(
              'weight' => 3,
            ),
            'filtered_html' => array(
              'weight' => 1,
            ),
            'full_html' => array(
              'weight' => 0,
            ),
            'menu_lateral_interior' => array(
              'weight' => 0,
            ),
            'php_code' => array(
              'weight' => 2,
            ),
            'plain_text' => array(
              'weight' => 5,
            ),
          ),
        ),
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-p_ginas_frontales-field_titulo_pf'.
  $field_instances['taxonomy_term-p_ginas_frontales-field_titulo_pf'] = array(
    'bundle' => 'p_ginas_frontales',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_titulo_pf',
    'label' => 'Título',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ace_editor' => 'ace_editor',
          'ds_code' => 'ds_code',
          'filtered_html' => 'filtered_html',
          'full_html' => 'full_html',
          'menu_lateral_interior' => 'menu_lateral_interior',
          'php_code' => 'php_code',
          'plain_text' => 'plain_text',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ace_editor' => array(
              'weight' => 4,
            ),
            'ds_code' => array(
              'weight' => 3,
            ),
            'filtered_html' => array(
              'weight' => 1,
            ),
            'full_html' => array(
              'weight' => 0,
            ),
            'menu_lateral_interior' => array(
              'weight' => 0,
            ),
            'php_code' => array(
              'weight' => 2,
            ),
            'plain_text' => array(
              'weight' => 5,
            ),
          ),
        ),
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Punto de acceso');
  t('Título');

  return $field_instances;
}
