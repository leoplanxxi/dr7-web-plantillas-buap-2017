<?php

//opcache_reset();

function buapCore_drush_command(){
    $items['buap-nombre-sitio'] = array(
        'description' => 'Descripción: Cambia el nombre de sitio principal.',
        'aliases' => array('bns'),
        'examples' => array(
            'drush buap-nombre-sitio "Mi nuevo sitio"' => 'Cambia el nombre del sitio principal a "Mi nuevo sitio".',
        ),
        'callback' => 'cambiarNombre',
    );

    return $items;
}

function cambiarNombre(){
    $args = func_get_args();
    if($args){
        db_update('field_data_field_nombre_del_sitio')
            ->fields(array(
                'field_nombre_del_sitio_value' => $args[0],
            ))->execute();
        drush_print("\n".'Nombre actualizado correctamente.'."\n");
    }
    else{
        drush_print("\n".'Falta especificar nombre.'."\n");
    }

}

