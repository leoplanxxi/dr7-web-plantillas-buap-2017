<?php

function buapCore_form($form, &$form_state){

  $js = db_query('SELECT field_javascript_value FROM {field_data_field_javascript}')->fetchField();
 
  $form['infojs'] = array(
    '#title' => t('Indicaciones'),
    '#markup' => '<p>Si usted necesita especificar hojas de estilo (CSS) personalizadas, puede hacerlo desde el siguiente campo. Estas hojas de estilo se aplican en todo el sitio.</p>',
  );

  $form['js'] = array(
    '#title' => t('JavaScript Personalizado'),
    '#type' => 'textarea',
    '#default_value' => $js,
    "#attributes" => array("class" => array("ace-enabled")),
  );

  $css = db_query('SELECT field_css_value FROM {field_data_field_css}')->fetchField();

  $form['infocss'] = array(
    '#title' => t('Indicaciones'),
    '#markup' => '<br>Si usted necesita especificar scripts (JavaScript) personalizados, puede hacerlo desde el siguiente campo. Estos se aplican en todo el sitio.',
  );

  $form['css'] = array(
    '#title' => t('CSS Personalizado'),
    '#type' => 'textarea',
    '#default_value' => $css,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Guardar'),
  );
  return $form;
}



function buapCore_form_submit($form, &$form_state){

  $js = $form_state['values']['js'];

  $act1 = db_update('field_data_field_javascript')
  ->fields(array(
    'field_javascript_value' => $js,
  ))
  ->execute();

  $css = $form_state['values']['css'];

  $act1 = db_update('field_data_field_css')
  ->fields(array(
    'field_css_value' => $css,
  ))
  ->execute();

  drupal_set_message(t('Your configuration has been saved.'));

}