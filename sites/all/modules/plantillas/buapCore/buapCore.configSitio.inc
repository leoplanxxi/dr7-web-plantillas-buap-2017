<?php

function buapCore_form($form, &$form_state) {

  // var_dump(config_pages_get('configuraci_n_de_sitio', 'field_img_izquierda_superpanel'));
  // die();

  $pe1 = db_query('SELECT field_peso_1_columna_value FROM {field_data_field_peso_1_columna}')->fetchField();
  $p1 = $pe1;
  $pe2 = db_query('SELECT field_peso_2_columnas_value FROM {field_data_field_peso_2_columnas}')->fetchField();
  $p2 = $pe2;
  $pe3 = db_query('SELECT field_peso_3_columnas_value FROM {field_data_field_peso_3_columnas}')->fetchField();
  $p3 = $pe3;
  $pe4 = db_query('SELECT field_peso_4_columnas_value FROM {field_data_field_peso_4_columnas}')->fetchField();
  $p4 = $pe4;
  $pe2v = db_query('SELECT field_peso_2_cols_var_value FROM {field_data_field_peso_2_cols_var}')->fetchField();
  $p2v = $pe2v;
  $pe3v = db_query('SELECT field_peso_3_cols_var_value FROM {field_data_field_peso_3_cols_var}')->fetchField();
  $p3v = $pe3v;
  $pe4v = db_query('SELECT field_peso_4_cols_var_value FROM {field_data_field_peso_4_cols_var}')->fetchField();
  $p4v = $pe4v;

  echo "<br>";
  $c1 = db_query('SELECT field_desplegar_1_columna_value FROM {field_data_field_desplegar_1_columna}')->fetchField();
  $ch1 = $c1;
  $c2 = db_query('SELECT field_desplegar_2_columnas_value FROM {field_data_field_desplegar_2_columnas}')->fetchField();
  $ch2 = $c2;
  $c3 = db_query('SELECT field_desplegar_3_columnas_value FROM {field_data_field_desplegar_3_columnas}')->fetchField();
  $ch3 = $c3;
  $c4 = db_query('SELECT field_desplegar_4_columnas_value FROM {field_data_field_desplegar_4_columnas}')->fetchField();
  $ch4 = $c4;

  $ns = db_query('SELECT field_nombre_del_sitio_value FROM {field_data_field_nombre_del_sitio}')->fetchField();
  $nombre = $ns;

  $d = db_query('SELECT field_direcci_n_sitio_value FROM {field_data_field_direcci_n_sitio}')->fetchField();
  $direccion = $d;

  $n = db_query('SELECT field_desplegar_panel_noticias_d_value FROM {field_data_field_desplegar_panel_noticias_d}')->fetchField();
  $noticias = $n;

  $p = db_query('SELECT field_mostrar_pesos_en_recursos_value FROM {field_data_field_mostrar_pesos_en_recursos}')->fetchField();
  $pesos = $p;

  $an = db_query('SELECT field_id_analytics_value FROM {field_data_field_id_analytics}')->fetchField();
  $analytics = $an;

  // $i_i = db_query('SELECT field_img_izquierda_superpanel_fid FROM {field_data_field_img_izquierda_superpanel}')->fetchField();

  $form['nombre'] = array(
    '#title' => t('Nombre del sitio'),
    '#type' => 'textfield',
    '#default_value' => $nombre,
  );

  $form['direccion'] = array(
    '#title' => t('Dirección'),
    '#type' => 'text_format',
    '#default_value' => $direccion,
  );

  // $form['img_izq'] = array(
  //   '#type' => 'managed_file',
  //   '#title' => t('Imagen Izquierda Superpanel'),
  //   '#default_value' => array('165'),
  //   '#upload_location' => 'public://',
  //   '#upload_validators' => array(
  //     'file_validate_extensions' => array('gif png jpg jpeg'),
  //   ),
  // );

  $form['data_table'] = array(
    '#type' => 'fieldset',
    '#title' => t('Peso de paneles en la página principal'),
  );

  $form['noticias'] = array(
    '#type' => 'checkbox',
    '#title' => t('Desplegar panel de noticias en el sitio'),
    '#default_value' => $noticias,
  );

  $form['mostrar_pesos'] = array(
    '#type' => 'checkbox',
    '#title' => t('Mostrar pesos en recursos'),
    '#default_value' => $pesos,
  );


  /*$form['pesos'] = array (
    '#type' => 'checkbox',
    '#title' => t('Mostrar pesos en recursos');
    '#default_value' => $pesos,
  );*/

  // Collect your data.
  $data = array(
    'some-id-1' => array(
      'enable' => $ch1,
      'default' => TRUE,
      'weight' => $p1,
      'name' => 'Sección de 1 columna',
      'description' => 'some description text',
    ),
    'some-id-2' => array(
      'enable' => $ch2,
      'default' => FALSE,
      'weight' => $p2,
      'name' => 'Sección de 2 columnas',
      'description' => 'more description text',
    ),
    'some-id-2v' => array(
 
      'default' => FALSE,
      'weight' => $p2v,
      'name' => 'Sección de 2 columnas (alto variable)',
      'description' => 'more description text',
    ),
    'some-id-3' => array(
      'enable' => $ch3,
      'default' => TRUE,
      'weight' => $p3,
      'name' => 'Sección de 3 columnas',
      'description' => 'mooore description text',
    ),
    'some-id-3v' => array(

      'default' => TRUE,
      'weight' => $p3v,
      'name' => 'Sección de 3 columnas (alto variable)',
      'description' => 'mooore description text',
    ),
    'some-id-4' => array(
      'enable' => $ch4,
      'default' => TRUE,
      'weight' => $p4,
      'name' => 'Sección de 4 columnas',
      'description' => 'mooore description text',
    ),
    'some-id-4v' => array(
   
      'default' => TRUE,
      'weight' => $p4v,
      'name' => 'Sección de 4 columnas (alto variable)',
      'description' => 'mooore description text',
    ),
  );

  // Sort the rows.
  uasort($data, '_buapCore_form_weight_arraysort');

  // Build the rows.
  foreach ($data as $id => $entry) {

    // Build the table rows.
    $rows[$id] = array(
      'data' => array(
        // Cell for the cross drag&drop element.
        array('class' => array('entry-cross')),
        // Weight item for the tabledrag.
        array('data' => array(
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#title_display' => 'invisible',
          '#default_value' => $entry['weight'],
          '#parents' => array('data_table', $id, 'weight'),
          '#attributes' => array(
            'class' => array('entry-order-weight'),
          ),
        )),
        // Enabled checkbox.
        array('data' => array(
          '#type' => 'checkbox',
          '#title' => t('Enable'),
          '#title_display' => 'invisible',
          '#default_value' => $entry['enable'],
          '#parents' => array('data_table', $id, 'enabled'),
        )),
    check_plain($entry['name']),
),
'class' => array('draggable'),
);
// Build rows of the form elements in the table.
$row_elements[$id] = array(
  'weight' => &$rows[$id]['data'][1]['data'],
  'enabled' => &$rows[$id]['data'][2]['data'],
  /*'default' => &$rows[$id]['data'][3]['data'],
  'name' => &$rows[$id]['data'][3]['data'],*/
);
}

// Add the table to the form.
$form['data_table']['table'] = array(
  '#theme' => 'table',
  // The row form elements need to be processed and build,
  // therefore pass them as element children.
  'elements' => $row_elements,
  '#header' => array(
    // We need two empty columns for the weigth field and the cross.
    array('data' => NULL, 'colspan' => 2),
    t('Enabled'),
    t('Description'),
  ),
  '#rows' => $rows,
  '#empty' => t('There are no entries available.'),
  '#attributes' => array('id' => 'entry-order'),
);
drupal_add_tabledrag('entry-order', 'order', 'sibling', 'entry-order-weight');

$form['analytics'] = array(
  '#title' => t('ID Google Analytics'),
  '#type' => 'textfield',
  '#default_value' => $analytics,
    '#description' => t('Introduzca el ID que usted utiliza para llevar analíticas en Google Analytics. Este ID tiene el siguiente formato: UA-000000-2. El sitio introduce de manera automática los scripts necesarios para que ésto funcione.'),
  '#attributes' => array("readonly" => "readonly",),
);

  $form['infocss'] = array(
    '#title' => t('Indicaciones'),
    '#markup' => '<script>			document.getElementById("edit-data-table-some-id-2v-enabled").style.display = "none"; document.getElementById("edit-data-table-some-id-3v-enabled").style.display = "none"; document.getElementById("edit-data-table-some-id-4v-enabled").style.display = "none";</script>',
  );

$form['submit_button'] = array(
  '#type' => 'submit',
  '#value' => t('Guardar'),
);

return $form;
}

function _buapCore_form_weight_arraysort($a, $b) {
  if (isset($a['weight']) && isset($b['weight'])) {
    return $a['weight'] < $b['weight'] ? -1 : 1;
  }
  return 0;
}

/*function buapCore_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {		
		case 'buapcore_form':

    		$form['#attached']['js'] = array(
          'data' => drupal_get_path('module', 'buapCore') . '/script.js',
          'type' => 'file', 
      		);
        break;
    }
	
}*/

function buapCore_form_submit($form, &$form_state) {
 
    // DRAGGABLE TABLE PESOS RECURSOS
    $peso1 = intval($form_state['values']['data_table']['some-id-1']['weight']);
    $peso2 = intval($form_state['values']['data_table']['some-id-2']['weight']);
    $peso3 = intval($form_state['values']['data_table']['some-id-3']['weight']);
    $peso4 = intval($form_state['values']['data_table']['some-id-4']['weight']);
    $peso2v = intval($form_state['values']['data_table']['some-id-2v']['weight']);
    $peso3v = intval($form_state['values']['data_table']['some-id-3v']['weight']);
    $peso4v = intval($form_state['values']['data_table']['some-id-4v']['weight']);

    // CHECK ACTIVADO/DESACTIVADO
    $check1 = intval($form_state['values']['data_table']['some-id-1']['enabled']);
    $check2 = intval($form_state['values']['data_table']['some-id-2']['enabled']);
    $check3 = intval($form_state['values']['data_table']['some-id-3']['enabled']);
    $check4 = intval($form_state['values']['data_table']['some-id-4']['enabled']);

    // NOMBRE DEL SITIO
    $nombre_sitio = $form_state['values']['nombre'];

    // ID Analytics
    $google_analytics = $form_state["values"]["analytics"];

    // DIRECCIÓN
    $dir = $form_state['values']['direccion']['value'];

    // DESPLEGAR NOTICIAS
    $desp_not = $form_state['values']['noticias'];

    // MOSTRAR PESOS EN LOS RECURSOS
    $most_pesos = $form_state['values']['mostrar_pesos'];

    // ACTUALIZACIÓN DE BASE DE DATOS CON LOS
    // VALORES OBTENIDOS DEL ESTADO ACTUAL DEL FORMULARIO

    //PESOS
    $act1 = db_update('field_data_field_peso_1_columna')
    ->fields(array(
      'field_peso_1_columna_value' => $peso1,
    ))
    ->execute();
    $act1 = db_update('field_data_field_peso_2_columnas')
    ->fields(array(
      'field_peso_2_columnas_value' => $peso2,
    ))
    ->execute();
    $act1 = db_update('field_data_field_peso_3_columnas')
    ->fields(array(
      'field_peso_3_columnas_value' => $peso3,
    ))
    ->execute();
    $act1 = db_update('field_data_field_peso_4_columnas')
    ->fields(array(
      'field_peso_4_columnas_value' => $peso4,
    ))
    ->execute();
    $act1 = db_update('field_data_field_peso_2_cols_var')
    ->fields(array(
      'field_peso_2_cols_var_value' => $peso2v,
    ))
    ->execute();
    $act1 = db_update('field_data_field_peso_3_cols_var')
    ->fields(array(
      'field_peso_3_cols_var_value' => $peso3v,
    ))
    ->execute();
    $act1 = db_update('field_data_field_peso_4_cols_var')
    ->fields(array(
      'field_peso_4_cols_var_value' => $peso4v,
    ))
    ->execute();

    //MOSTRAR U OCULTAR
    $act1 = db_update('field_data_field_desplegar_1_columna')
    ->fields(array(
      'field_desplegar_1_columna_value' => $check1,
    ))
    ->execute();
    $act1 = db_update('field_data_field_desplegar_2_columnas')
    ->fields(array(
      'field_desplegar_2_columnas_value' => $check2,
    ))
    ->execute();
    $act1 = db_update('field_data_field_desplegar_3_columnas')
    ->fields(array(
      'field_desplegar_3_columnas_value' => $check3,
    ))
    ->execute();
    $act1 = db_update('field_data_field_desplegar_4_columnas')
    ->fields(array(
      'field_desplegar_4_columnas_value' => $check4,
    ))
    ->execute();

    
    $act1 = db_update('field_data_field_nombre_del_sitio')
    ->fields(array(
      'field_nombre_del_sitio_value' => $nombre_sitio,
    ))
    ->execute();

    $act1 = db_update('field_data_field_direcci_n_sitio')
    ->fields(array(
      'field_direcci_n_sitio_value' => $dir,
    ))
    ->execute();
    $act1 = db_update('field_data_field_desplegar_panel_noticias_d')
    ->fields(array(
      'field_desplegar_panel_noticias_d_value' => $desp_not,
    ))
    ->execute();
    $act1 = db_update('field_data_field_mostrar_pesos_en_recursos')
    ->fields(array(
      'field_mostrar_pesos_en_recursos_value' => $most_pesos,
    ))
    ->execute();
    $act1 = db_update('field_data_field_id_analytics')
    ->fields(array(
      'field_id_analytics_value' => $google_analytics,
    ))
    ->execute();

    // IMÁGENES SUPERPANEL
    // $fid = $form_state['values']['img_izq'];
    // $act1 = db_update('field_data_field_img_izquierda_superpanel')
    // ->fields(array(
    //   'field_img_izquierda_superpanel_fid' => $fid,
    // ))
    // ->execute();
    // $file = file_load($form_state['values']['img_izq']);
    // $file->status = FILE_STATUS_PERMANENT;
    // $file_saved =file_save($file);
    // file_usage_add($file_saved, 'buapCore_form', 'buapCore_form', $file_saved->fid);

    drupal_flush_all_caches();
    drupal_set_message(t('Your configuration has been saved.'));

}
