Drupal.locale = { 'pluralFormula': function ($n) { return Number(($n!=1)); }, 'strings': {"":{"An AJAX HTTP error occurred.":"Se produjo un error HTTP AJAX.","HTTP Result Code: !status":"C\u00f3digo de Resultado HTTP: !status","An AJAX HTTP request terminated abnormally.":"Una solicitud HTTP de AJAX termin\u00f3 de manera anormal.","Debugging information follows.":"A continuaci\u00f3n se detalla la informaci\u00f3n de depuraci\u00f3n.","Path: !uri":"Ruta: !uri","StatusText: !statusText":"StatusText: !statusText","ResponseText: !responseText":"ResponseText: !responseText","ReadyState: !readyState":"ReadyState: !readyState","CustomMessage: !customMessage":"CustomMessage: !customMessage","All":"Todo","Re-order rows by numerical weight instead of dragging.":"Reordenar las filas por peso num\u00e9rico en lugar de arrastrar.","Show row weights":"Mostrar pesos de la fila","Hide row weights":"Ocultar pesos de la fila","Drag to re-order":"Arrastre para reordenar","Changes made in this table will not be saved until the form is submitted.":"Los cambios realizados en esta tabla no se guardar\u00e1n hasta que se env\u00ede el formulario","Hide":"Ocultar","Show":"Mostrar","Edit":"Editar","Add":"Agregar","Customize dashboard":"Personalizar panel de control","Status":"Estado","Description":"Descripci\u00f3n","Owner":"Propietario","Not published":"No publicado","Size":"Tama\u00f1o","(active tab)":"(solapa activa)","Enabled":"Activado","This field is required.":"Este campo es obligatorio.","Tue":"Mar","Wed":"Mi\u00e9","Thu":"Jue","Mon":"Lun","Sun":"Dom","Configure":"Configurar","Done":"Hecho","Next":"Siguiente","Disabled":"Desactivado","Sunday":"Domingo","Monday":"Lunes","Tuesday":"Martes","Wednesday":"Mi\u00e9rcoles","Thursday":"Jueves","Friday":"Viernes","Saturday":"S\u00e1bado","Filename":"Nombre de archivo","Upload":"Subir al servidor","1 hour":"1 hora","@count hours":"@count horas","1 day":"1 d\u00eda","@count days":"@count d\u00edas","N\/A":"N\/D","OK":"OK","Prev":"Previo","Fri":"Vie","Sat":"S\u00e1b","January":"Enero","February":"Febrero","March":"Marzo","April":"Abril","May":"Mayo","June":"Junio","July":"Julio","August":"Agosto","September":"Septiembre","October":"Octubre","November":"Noviembre","December":"Diciembre","Select all rows in this table":"Seleccionar todas las filas de esta tabla","Deselect all rows in this table":"Quitar la selecci\u00f3n a todas las filas de esta tabla","Today":"Hoy","Jan":"Ene","Feb":"Feb","Mar":"Mar","Apr":"Abr","Jun":"Jun","Jul":"Jul","Aug":"Ago","Sep":"Sep","Oct":"Oct","Nov":"Nov","Dec":"Dic","Su":"Do","Mo":"Lu","Tu":"Ma","We":"Mi","Th":"Ju","Fr":"Vi","Sa":"Sa","Please wait...":"Espere, por favor...","1 year":"1 a\u00f1o","@count years":"@count a\u00f1os","1 week":"1 semana","@count weeks":"@count semanas","1 min":"1 min","@count min":"@count mins","1 sec":"1 seg","@count sec":"@count segs","mm\/dd\/yy":"mm\/dd\/yy","1 month":"1 mes","@count months":"@count meses","Only files with the following extensions are allowed: %files-allowed.":"S\u00f3lo se permiten archivos con las siguientes extensiones: %files-allowed.","By @name on @date":"Por @name en @date","By @name":"Por @name","Not in menu":"No est\u00e1 en un men\u00fa","Alias: @alias":"Alias: @alias","No alias":"Sin alias","0 sec":"0 seg","New revision":"Nueva revisi\u00f3n","The changes to these blocks will not be saved until the \u003Cem\u003ESave blocks\u003C\/em\u003E button is clicked.":"Los cambios sobre estos bloques no se guardar\u00e1n hasta que no pulse el bot\u00f3n \u003Cem\u003EGuardar bloques\u003C\/em\u003E.","This permission is inherited from the authenticated user role.":"Este permiso se hereda del rol de usuario registrado.","No revision":"Sin revisi\u00f3n","@number comments per page":"@number comentarios por p\u00e1gina","Requires a title":"Necesita un t\u00edtulo","Not restricted":"Sin restricci\u00f3n","Not customizable":"No personalizable","Restricted to certain pages":"Restringido a algunas p\u00e1ginas","The block cannot be placed in this region.":"El bloque no se puede colocar en esta regi\u00f3n.","Hide summary":"Ocultar resumen","Edit summary":"Editar resumen","Don\u0027t display post information":"No mostrar informaci\u00f3n del env\u00edo","The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.":"El archivo seleccionado %filename no puede ser subido. Solo se permiten archivos con las siguientes extensiones: %extensions.","Autocomplete popup":"Ventana emergente con autocompletado","Searching for matches...":"Buscando coincidencias","The response failed verification so will not be processed.":"La respuesta de verificaci\u00f3n fall\u00f3 por lo que no ser\u00e1 procesado.","The callback URL is not local and not trusted: !url":"La URL de llamada no es local y no es confiable: !url","Please select a file.":"Seleccione un documento, por favor.","You are not allowed to operate on more than %num files.":"No tiene permiso para actuar sobre m\u00e1s de %num documentos.","Please specify dimensions within the allowed range that is from 1x1 to @dimensions.":"Especifique unas dimensiones dentro de las permitidas, por favor. Eso va desde 1 \u00d7 1 a @dimensions.","%filename is not an image.":"%filename no es una imagen.","Insert file":"Insertar archivo","Change view":"Cambiar vista","Directory":"Directorio"}} };
;/*})'"*/
;/*})'"*/

/* see Issue "autoresize" http://drupal.org/node/360549 */
(function ($) {
  Drupal.behaviors.iframeModule = {
    attach: function(context, settings) {
      $('iframe.autoresize').each(function() {
        var offsetHeight = 20;
        var thisIframe = $(this);
        var iframeWaitInterval;

        function resizeHeight(iframe) {
          if ($(iframe).length) { /* Iframe yet loaded ? */
            var iframeDoc = $(iframe)[0].contentDocument || $(iframe)[0].contentWindow.document;
            var contentheight = 0;

            try {
              contentheight = $(iframeDoc).find('body').height();
            } catch (e) {
              elem = $(iframe)[0];
              msg = $('<p><small>(' + Drupal.t('Iframe URL is not from the same domain - autoresize not working.') + ')</small></p>');
              $(elem).after(msg);
              clearInterval(iframeWaitInterval);
            }

            if (contentheight > 0) {
              clearInterval(iframeWaitInterval);
              try {
                var frameElement = $(iframe)[0].frameElement || $(iframe)[0];
                frameElement.style.height = (contentheight + offsetHeight) + 'px';
                frameElement.scrolling = 'no';
              } catch (e) {
                /* here, ist not an correctable error */
              }
            }
          }
        }
        var delayedResize = function() {
            resizeHeight(thisIframe);
        }

        iframeWaitInterval = setInterval(delayedResize, 300);
        //setTimeout(delayedResize, 300);
        //resizeHeight(thisIframe);
      });
    }
  }
})(jQuery);



;/*})'"*/
;/*})'"*/
/**
 * @file
 * Behaviors for Devel.
 */

(function ($) {

/**
 * Attaches double-click behavior to toggle full path of Krumo elements.
 *
 * @type {Drupal~behavior}
 */
Drupal.behaviors.devel = {
  attach: function (context, settings) {

    // Path
    // Add hint to footnote
    $('.krumo-footnote .krumo-call', context).once().before('<img style="vertical-align: middle;" title="Click to expand. Double-click to show path." src="' + settings.basePath + 'misc/help.png"/>');

    var krumo_name = [];
    var krumo_type = [];

    function krumo_traverse(el) {
      krumo_name.push($(el).html());
      krumo_type.push($(el).siblings('em').html().match(/\w*/)[0]);

      if ($(el).closest('.krumo-nest').length > 0) {
        krumo_traverse($(el).closest('.krumo-nest').prev().find('.krumo-name'));
      }
    }

    $('.krumo-child > div:first-child', context).once('krumo_path',
    function() {
      $('.krumo-child > div:first-child', context).dblclick(
      function(e) {
        if ($(this).find('> .krumo-php-path').length > 0) {
          // Remove path if shown.
          $(this).find('> .krumo-php-path').remove();
        }
        else {
          // Get elements.
          krumo_traverse($(this).find('> a.krumo-name'));

          // Create path.
          var krumo_path_string = '';
          for (var i = krumo_name.length - 1; i >= 0; --i) {
            // Start element.
            if ((krumo_name.length - 1) == i)
              krumo_path_string += '$' + krumo_name[i];

            if (typeof krumo_name[(i-1)] !== 'undefined') {
              if (krumo_type[i] == 'Array') {
                krumo_path_string += "[";
                if (!/^\d*$/.test(krumo_name[(i-1)]))
                  krumo_path_string += "'";
                krumo_path_string += krumo_name[(i-1)];
                if (!/^\d*$/.test(krumo_name[(i-1)]))
                  krumo_path_string += "'";
                krumo_path_string += "]";
              }
              if (krumo_type[i] == 'Object')
                krumo_path_string += '->' + krumo_name[(i-1)];
            }
          }
          $(this).append('<div class="krumo-php-path" style="font-family: Courier, monospace; font-weight: bold;">' + krumo_path_string + '</div>');

          // Reset arrays.
          krumo_name = [];
          krumo_type = [];
        }
      });
    });

    // Events
    $('.krumo-element').once('krumo-events', function() {
      $(this).click(function() {
        krumo.toggle(this);
      }).mouseover(function() {
        krumo.over(this);
      }).mouseout(function() {
        krumo.out(this);
      });
    });// End krumo-events .once
  }// End attach.
};// End behaviors.devel.

})(jQuery);// end outer function

;/*})'"*/
;/*})'"*/
